﻿
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Utils;
namespace Models
{
    public class TDLObject : ITDLObject
    {
        private ScriptDescriptor m_ScriptDescriptor = new ScriptDescriptor();
        private List<string> m_BearingList = new List<string>();

        #region Properties
        /// <summary>
        /// Title - Truss ID: String
        /// </summary>
        public string Title { get; set; }

        public string Type { get; set; }

        public string TrussType { get; set; }

        public string FileVersion { get; set; }

        public string Version { get; set; }

        public string CompatibilityVersion { get; set; }

        public string Thickness { get; set; }

        public int Ply { get; set; }

        public int Quantity { get; set; }

        public double Span { get; set; }

        public double ProductionSpan { get; set; }

        public double Spacing { get; set; }

        public string TCProfile { get; set; }

        public string BCProfile { get; set; }

        public string WebProfile { get; set; }

        public string Left_Cantilever { get; set; }

        public string Right_Cantilever { get; set; }

        public string Left_Stub { get; set; }

        public string Right_Stub { get; set; }

        public double Left_OH { get; set; }

        public double Right_OH { get; set; }

        public string OHLProfile { get; set; }

        public string OHRProfile { get; set; }

        public string Left_OHA { get; set; }

        public string Right_OHA { get; set; }

        public string Left_EndDesc { get; set; }

        public string Right_EndDesc { get; set; }

        public string Left_EndDescA { get; set; }

        public string Right_EndDescA { get; set; }

        public string Gable { get; set; }

        public string Attic { get; set; }

        public List<string> BearingList 
        { 
            get
            {
                return m_BearingList;
            }
        }

        public string LoadTemplate { get; set; }

        public string AdditionalCommand { get; set; }

        public double Left_HeelHeight { get; set; }

        public double Right_HeelHeight { get; set; }

        public double OverallHeight { get; set; }

        public IScriptDescriptor ScriptDescriptor
        {
            get
            {
                m_ScriptDescriptor = new ScriptDescriptor();
                m_ScriptDescriptor.Thickness = @"thk: " + Thickness;                    
                m_ScriptDescriptor.Ply = @"plys: " + Ply.ToString();
                m_ScriptDescriptor.Quantity = @"mfg: " + Quantity.ToString();
                m_ScriptDescriptor.Span = @"span: " + Util.ToFeetInchSix(Span);
                m_ScriptDescriptor.ProductionSpan = @"prdspn: " + Util.ToFeetInchSix(Span);
                m_ScriptDescriptor.Spacing = @"spacing: " + Util.ToFeetInchSix(Spacing);
                m_ScriptDescriptor.TCProfile = @"tc: " + TCProfile;
                m_ScriptDescriptor.BCProfile = @"bc: " + BCProfile;
                m_ScriptDescriptor.WebProfile = @"web: " + WebProfile;
                m_ScriptDescriptor.Left_Cantilever = @"cantl: " + Left_Cantilever;
                m_ScriptDescriptor.Right_Cantilever = @"cantr: " + Right_Cantilever;
                m_ScriptDescriptor.Left_Stub = @"stubl: " + Left_Stub;
                m_ScriptDescriptor.Right_Stub = @"stubr: " + Right_Stub;
                m_ScriptDescriptor.Left_OH = @"ohl: " + OHLProfile;
                m_ScriptDescriptor.Right_OH = @"ohr: " + OHRProfile;
                m_ScriptDescriptor.Left_OHA = @"ohla: " + Left_OHA;
                m_ScriptDescriptor.Right_OHA = @"ohra: " + Right_OHA;
                m_ScriptDescriptor.Left_EndDesc = @"edl: " + Left_EndDesc;
                m_ScriptDescriptor.Right_EndDesc = @"edr: " + Right_EndDesc;
                m_ScriptDescriptor.Left_EndDescA = @"edla: " + Left_EndDescA;
                m_ScriptDescriptor.Right_EndDescA = @"edra: " + Right_EndDescA;
                m_ScriptDescriptor.Gable = @"gbl: " + Gable;
                m_ScriptDescriptor.Attic = @"at: " + Attic;
                m_ScriptDescriptor.BearingList = GetBearingValues();
                m_ScriptDescriptor.LoadTemplate = @"LoadTemplate: ";
                m_ScriptDescriptor.AdditionalCommand = @"ac: ";

                return m_ScriptDescriptor;
            }
        }

        public ISettingsDescriptor SettingstDescriptor { get; set; }

        #endregion Properties

        #region Methods

        private string GetBearingValues()
        {
            return string.Join(Environment.NewLine, BearingList);
        }

        private XmlDocument GetSerializableObj()
        {
            var doc = new XmlDocument();
            var tdlObject = doc.CreateElement("tdlObject");
            doc.AppendChild(tdlObject);

            tdlObject.SetAttribute("Title", Title);
            tdlObject.SetAttribute("FileVersion", FileVersion);
            tdlObject.SetAttribute("Version", Version);
            tdlObject.SetAttribute("CompatibilityVersion", CompatibilityVersion);
            tdlObject.SetAttribute("Type", Type);

            var scripts = doc.CreateElement("Script");
            scripts.InnerText = ScriptDescriptor.ToString();
            tdlObject.AppendChild(scripts);

            if (SettingstDescriptor != null && SettingstDescriptor.Settings != null && SettingstDescriptor.Settings.Any())
            {
                var settings = doc.CreateElement("Settings");
                foreach (var item in SettingstDescriptor.Settings)
                {
                    var node = doc.CreateElement(item.Item1);
                    var value = doc.CreateAttribute("Value");
                    value.Value = item.Item2;
                    node.Attributes.Append(value);
                    settings.AppendChild(node);
                }

                tdlObject.AppendChild(settings);
            }            

            return doc;
        }

        public void WriteTDLFile(string outputPath)
        {
            Util.SerializeToTDLFile(GetSerializableObj(), outputPath, this.Title + ".tdlTruss");
        }

        #endregion Methods              
    }
}
