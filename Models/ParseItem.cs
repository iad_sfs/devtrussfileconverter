﻿using Interfaces;
using System.IO;
namespace Models
{
    public class ParseItem : IContextObject
    {                        
        public bool IsRejected { get; set; }

        public bool IsProcessFailed { get; set; }

        public string FilePath { get; set; }

        public string FileVersion { get; set; }

        public RejectCause CauseOfReject { get; set; }

        #region Constructors

        public ParseItem(string path)
        {
            FilePath = path;
        }

        #endregion Constructors

        public string FileNameNoExtension
        {
            get
            {
                if (string.IsNullOrWhiteSpace(FilePath))
                {
                    return string.Empty;
                }

                return Path.GetFileNameWithoutExtension(FilePath);
            }
        }

        public string FileName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(FilePath))
                {
                    return string.Empty;
                }

                return Path.GetFileName(FilePath);
            }
        }

        public string DirName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(FilePath))
                {
                    return string.Empty;
                }

                return Path.GetDirectoryName(FilePath);
            }
        }

        public string Extension
        {
            get
            {
                if (string.IsNullOrWhiteSpace(FilePath))
                {
                    return string.Empty;
                }

                return Path.GetExtension(FilePath);
            }
        }        
    }
}
