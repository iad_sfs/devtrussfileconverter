﻿using Interfaces;
using System;
using System.Collections.Generic;

namespace Models
{
    public class ScriptDescriptor : IScriptDescriptor
    {
        #region Properties

        public string Thickness { get; set; }

        public string Ply { get; set; }

        public string Quantity { get; set; }

        public string Span { get; set; }

        public string ProductionSpan { get; set; }

        public string Spacing { get; set; }

        public string TCProfile { get; set; }

        public string BCProfile { get; set; }

        public string WebProfile { get; set; }

        public string Left_Cantilever { get; set; }

        public string Right_Cantilever { get; set; }

        public string Left_Stub { get; set; }

        public string Right_Stub { get; set; }

        public string Left_OH { get; set; }

        public string Right_OH { get; set; }

        public string Left_OHA { get; set; }

        public string Right_OHA { get; set; }

        public string Left_EndDesc { get; set; }

        public string Right_EndDesc { get; set; }

        public string Left_EndDescA { get; set; }

        public string Right_EndDescA { get; set; }

        public string Gable { get; set; }

        public string Attic { get; set; }

        public string BearingList { get; set; }

        public string LoadTemplate { get; set; }

        public string AdditionalCommand { get; set; }

        public string Left_HeelHeight { get; set; }

        public string Right_HeelHeight { get; set; }

        public string OverallHeight { get; set; }

        // Add more here...

        #endregion Properties

        #region Methods

        public override string ToString()
        {
            List<string> values = new List<string>() 
                    {
                        Thickness,
                        Ply,
                        Quantity,
                        Span,
                        Spacing,
                        TCProfile,
                        BCProfile,
                        WebProfile,
                        Left_Cantilever,
                        Right_Cantilever,
                        Left_Stub,
                        Right_Stub,
                        Left_OH,
                        Right_OH,
                        Left_OHA,
                        Right_OHA,
                        Left_EndDesc,
                        Right_EndDesc,
                        Left_EndDescA,
                        Right_EndDescA,
                        Gable,
                        Attic,
                        BearingList,
                        LoadTemplate,
                        AdditionalCommand,
                    };

            values.RemoveAll(x => string.IsNullOrWhiteSpace(x));
            return string.Format("{0}{1}{2}", Environment.NewLine, string.Join(Environment.NewLine, values), Environment.NewLine);
        }

        #endregion Methods
    }
}
