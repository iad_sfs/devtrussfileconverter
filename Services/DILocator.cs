﻿
using GalaSoft.MvvmLight.Ioc;
using Interfaces;
using log4net;
using Microsoft.Practices.ServiceLocation;
using Models;
using System;
using System.Linq;
using System.Reflection;
using TDLConverter;

namespace Services
{    
    public class DILocator
    {
        #region Members

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static DILocator m_Instance;

        #endregion Members        

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the DIContainer class.
        /// </summary>
        private DILocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<IParser, Parser>();
            SimpleIoc.Default.Register<IConverter, TDLConverterCore>();
        }

        #endregion Constructors

        #region Properties

        public static DILocator Instance
        { 
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new DILocator();
                }

                return m_Instance;
            }
        }

        public IConverter ConverterCore
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IConverter>();
            }
        }

        #endregion Properties

        #region Methods

        public IContextObject CreateNewContextObject(string path)
        {
            try
            {
                if (!SimpleIoc.Default.IsRegistered<IContextObject>(path))
                {
                    SimpleIoc.Default.Register<IContextObject>(() => new ParseItem(path), path);
                }

                return ServiceLocator.Current.GetInstance<IContextObject>(path);
            }
            catch (Exception ex)
            {
                Log.Error("Failed to retrive ParseItem(path = " + path + ")", ex);
                return null;
            }

            
        }

        public void ClearAllContextObjects()
        {
            var registeredContextObjects = SimpleIoc.Default.GetAllCreatedInstances<IContextObject>().ToList();

            for (int i = 0 ; i < registeredContextObjects.Count() ; i++)
            {
                SimpleIoc.Default.Unregister(registeredContextObjects[i]);
            }         
        }

        public static void Cleanup()
        {
            SimpleIoc.Default.Reset();
        }

        #endregion Methods
    }    
}
