﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resources
{
    public class StudioVersion
    {
        public StudioVersion(string version)
        {
            var array = version.Split('.');

            if (array.Count() == 4)
            {
                MajorNumber = Convert.ToInt32(array[0]);
                MinorNumber = Convert.ToInt32(array[1]);
                BuildNumber = Convert.ToInt32(array[2]);
                RevisionNumber = Convert.ToInt32(array[3]);
            }            
        }

        public int MajorNumber { get; set; } = 0;
        public int MinorNumber { get; set; } = 0;
        public int BuildNumber { get; set; } = 0;
        public int RevisionNumber { get; set; } = 0;

        public bool IsEmpty
        {
            get
            {
                return MajorNumber == 0 && MinorNumber == 0 && BuildNumber == 0 && RevisionNumber == 0;
            }
        }

        public static bool operator ==(StudioVersion lhs, StudioVersion rhs)
        {
            bool status = false;
            if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber == rhs.MinorNumber && lhs.BuildNumber == rhs.BuildNumber && lhs.RevisionNumber == rhs.RevisionNumber)
            {
                status = true;
            }
            return status;
        }

        public static bool operator !=(StudioVersion lhs, StudioVersion rhs)
        {
            bool status = false;

            if (lhs.MajorNumber != rhs.MajorNumber || lhs.MinorNumber != rhs.MinorNumber || lhs.BuildNumber != rhs.BuildNumber || lhs.RevisionNumber != rhs.RevisionNumber)
            {
                status = true;
            }
            return status;
        }

        public static bool operator <(StudioVersion lhs, StudioVersion rhs)
        {
            bool status = false;

            if (lhs.MajorNumber < rhs.MajorNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber < rhs.MinorNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber == rhs.MinorNumber && lhs.BuildNumber < rhs.BuildNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber == rhs.MinorNumber && lhs.BuildNumber == rhs.BuildNumber && lhs.RevisionNumber < rhs.RevisionNumber)
            {
                status = true;
            }

            return status;
        }

        public static bool operator >(StudioVersion lhs, StudioVersion rhs)
        {
            bool status = false;

            if (lhs.MajorNumber > rhs.MajorNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber > rhs.MinorNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber == rhs.MinorNumber && lhs.BuildNumber > rhs.BuildNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber == rhs.MinorNumber && lhs.BuildNumber == rhs.BuildNumber && lhs.RevisionNumber > rhs.RevisionNumber)
            {
                status = true;
            }

            return status;
        }

        public static bool operator <=(StudioVersion lhs, StudioVersion rhs)
        {
            bool status = false;

            if (lhs.MajorNumber <= rhs.MajorNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber <= rhs.MinorNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber == rhs.MinorNumber && lhs.BuildNumber <= rhs.BuildNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber == rhs.MinorNumber && lhs.BuildNumber == rhs.BuildNumber && lhs.RevisionNumber <= rhs.RevisionNumber)
            {
                status = true;
            }

            return status;
        }

        public static bool operator >=(StudioVersion lhs, StudioVersion rhs)
        {
            bool status = false;

            if (lhs.MajorNumber >= rhs.MajorNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber >= rhs.MinorNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber == rhs.MinorNumber && lhs.BuildNumber >= rhs.BuildNumber)
            {
                status = true;
            }
            else if (lhs.MajorNumber == rhs.MajorNumber && lhs.MinorNumber == rhs.MinorNumber && lhs.BuildNumber == rhs.BuildNumber && lhs.RevisionNumber >= rhs.RevisionNumber)
            {
                status = true;
            }

            return status;
        }

        public override string ToString()
        {
            return $"{ MajorNumber }" + "." + $"{MinorNumber}" + "." + $"{BuildNumber}" + "." + $"{RevisionNumber}" ;
        }

        public override bool Equals(object obj)
        {
            var objVersion = obj as StudioVersion;
            if (objVersion == null)
            {
                return false;
            }

            bool status = false;

            if (MajorNumber == objVersion.MajorNumber && MinorNumber == objVersion.MinorNumber && BuildNumber == objVersion.BuildNumber && RevisionNumber == objVersion.RevisionNumber)
            {
                status = true;
            }
            return status;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
