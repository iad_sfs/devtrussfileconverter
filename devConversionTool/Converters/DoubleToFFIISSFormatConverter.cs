﻿using System;
using System.Windows.Data;

namespace devConversionTool.Converters
{
    public class DoubleToFFIISSFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {            
            double inputValue = 0.0;

            if (value.GetType() == typeof(double))
            {
                inputValue = (double)value;
            }

            return Utils.Util.FormatFIS(inputValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
