﻿using Interfaces;
using System;
using System.Windows;
using System.Windows.Data;

namespace devConversionTool.Converters
{
    public class EnumToBooleanConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null && value.GetType().IsEnum)
            {
                if (parameter != null && parameter.GetType().IsEnum)
                {
                    return Enum.Equals(value, parameter);
                }

                string parameterString = parameter as string;

                if (parameterString == null)
                {
                    return DependencyProperty.UnsetValue;
                }

                BearingMaterialEnum parameterValue;

                if (Enum.TryParse(parameterString, out parameterValue))
                {
                    return Enum.Equals(value, parameterValue);
                }

                return DependencyProperty.UnsetValue;
            }
            else
            {
                return DependencyProperty.UnsetValue;
            }                
        }

        public object ConvertBack(object value, Type targetType, object trueValue, System.Globalization.CultureInfo culture)
        {
            if (value is bool && (bool)value)
            {
                return trueValue;
            }
            else
            {
                return DependencyProperty.UnsetValue;
            }                
        }

        #endregion
    }

}
