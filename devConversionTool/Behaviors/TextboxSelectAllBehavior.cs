﻿using System.Windows.Controls;
using System.Windows.Interactivity;

namespace devConversionTool.Behaviors
{
    class TextboxSelectAllBehavior : Behavior<TextBox>
    {
        protected override void OnAttached()
        {
            AssociatedObject.GotFocus += AssociatedObject_GotFocus;
            AssociatedObject.GotKeyboardFocus += AssociatedObject_GotFocus;
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.GotFocus -= AssociatedObject_GotFocus;
            AssociatedObject.GotKeyboardFocus -= AssociatedObject_GotFocus;
            base.OnDetaching();
        }

        void AssociatedObject_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            AssociatedObject.SelectAll();
        }
    }
}
