/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Resources"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using devConversionTool.Views;
using GalaSoft.MvvmLight.Ioc;
using Interfaces;
using Microsoft.Practices.ServiceLocation;

namespace devConversionTool.ViewModels
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        private static ViewModelLocator m_Instance;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        private ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<IDialogService, DialogService>();
            SimpleIoc.Default.Register<TDLConververVM>();            
        }

        #endregion Constructors

        #region Properties

        public static ViewModelLocator Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new ViewModelLocator();
                }

                return m_Instance;
            }
        }

        public TDLConververVM MainViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TDLConververVM>();                
            }
        }

        #endregion Properties

        #region Methods

        public static void Cleanup()
        {
            SimpleIoc.Default.Reset();
        }

        #endregion Methods
    }
}