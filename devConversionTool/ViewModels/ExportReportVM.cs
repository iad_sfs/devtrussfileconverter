﻿
using devConversionTool.Infrastructure;
using Resources;
using System.ComponentModel;
using Utils;
namespace devConversionTool
{
    public class ExportReportVM : ScreenViewModelBase, IDataErrorInfo
    {
        #region Members

        private string m_ReportName = @"Report";

        #endregion Members

        #region Constructors

        public ExportReportVM()
        {
            IsValidationEnabled = true;
        }

        #endregion Constructors

        #region Properties

        public string ReportName
        {
            get
            {
                return m_ReportName;
            }
            set
            {
                m_ReportName = value;
                RaisePropertyChanged(() => ReportName);
            }
        }

        public bool IsValid
        {
            get 
            {
                return ValidateFileName() == null;
            }
        }

        #endregion Properties

        #region Methods

        protected override bool ValidateModifications()
        {
            if (!IsValid)
            {                
                RaisePropertyChanged(null);
                return false;
            }

            return base.ValidateModifications();
        }

        protected override string ValidateProperty(string columnName)
        {
            if (columnName == "ReportName")
            {
                return ValidateFileName();
            }

            return null;
        }       

        private string ValidateFileName()
        {
            if (Util.HasIllegalFileNameChars(ReportName))
            {
                return Strings.FileNameNotValidErrorMessage;
            }

            if (string.IsNullOrWhiteSpace(ReportName))
            {
                return Strings.FileNameIsEmptyErrorMessage;
            }

            return null;
        }

        #endregion Methods
    }
}
