﻿using devConversionTool.Infrastructure;
using Interfaces;
using System.Collections.ObjectModel;
using System.Linq;

namespace devConversionTool.ViewModels
{
    public class ParseItemVM : ViewModelBase<IContextObject>
    {
        private ObservableCollection<ParseItemVM> m_SubNodes;
        private bool m_ReentrancyCheck;
        private bool? m_IsChecked;
        private string m_ProcessStatus;

        public ParseItemVM(IContextObject model)
        {
            Model = model;
        }

        public ParseItemVM ParentItem { get; set; }

        public string FilePath
        { 
            get
            {
                return Model.FilePath;
            }
        }

        public string Name 
        {
            get
            {
                return Model.FileName;
            }
        }

        public string ProcessStatus 
        {
            get
            {
                return m_ProcessStatus;
            }
            set
            {
                if (m_ProcessStatus != value)
                {
                    m_ProcessStatus = value;
                    RaisePropertyChanged(() => ProcessStatus);
                }
            }
        }
        
        public bool? IsChecked
        {
            get
            {
                return m_IsChecked;
            }
            set
            {
                if (m_IsChecked != value)
                {
                    if (m_ReentrancyCheck)
                    {
                        return;
                    }

                    m_ReentrancyCheck = true;
                    m_IsChecked = value;
                    UpdateCheckState();
                    RaisePropertyChanged(() => IsChecked);
                    m_ReentrancyCheck = false;
                }
            }
        }

        public ObservableCollection<ParseItemVM> SubNodes
        {
            get
            {
                if (m_SubNodes == null)
                {
                    m_SubNodes = new ObservableCollection<ParseItemVM>();
                }

                return m_SubNodes;
            }
        }

        private void UpdateChildrenCheckState()
        {
            foreach (var item in SubNodes)
            {
                if (IsChecked != null)
                {
                    item.IsChecked = IsChecked;
                }
            }
        }

        private void UpdateCheckState()
        {
            // update all children:
            if (SubNodes.Count != 0)
            {
                UpdateChildrenCheckState();
            }
            //update parent item
            if (ParentItem != null)
            {
                bool? parentIsChecked = ParentItem.DetermineCheckState();
                ParentItem.IsChecked = parentIsChecked;
            }
        }

        private bool? DetermineCheckState()
        {
            if (SubNodes.Count == 0)
            {
                return false;
            }

            bool allChildrenChecked = SubNodes.Count(x => x.IsChecked == true) == SubNodes.Count;
            if (allChildrenChecked)
            {
                return true;
            }

            bool allChildrenUnchecked = SubNodes.Count(x => x.IsChecked == false) == SubNodes.Count;
            if (allChildrenUnchecked)
            {
                return false;
            }

            return null;
        }
    }
}
