﻿
using devConversionTool.Infrastructure;
using Interfaces;
using log4net;
using Microsoft.Win32;
using Resources;
using Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Xml.Linq;
using Utils;

namespace devConversionTool.ViewModels
{
    public class TDLConververVM : ScreenViewModelBase
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private ParseItemVM m_RootNode;
        private int m_ProgressPercentage;
        private bool m_ShowWaitingLabel;
        private string m_JobPath;
        private string m_OutputPath;
        private string m_ConsoleOutPut;
        private bool m_AutoFillOutputFolder = true;
        private ObservableCollection<ITDLObject> m_ViewObjects;
        private List<ITDLObject> m_ProcessedObjects;
        private List<IContextObject> m_ItemsToProcess;
        private ProcessingStateEnum m_ProcessingState;
        private IConverter m_ConverterCore;
        private Stopwatch m_Stopwatch = new Stopwatch();

        private ICommand m_ClosingCommand;
        private ICommand m_InputFolderCommnad;
        private ICommand m_OutputFolderCommnad;
        private ICommand m_StopProcessCommand;
        private ICommand m_ViewProfileCommand;
        private ICommand m_ConvertCommand;
        private ICommand m_ExportReportCommand;
        private ICommand m_ClearAllConsoleOutputCommand;
        private ICommand m_CopyAllConsoleOutputCommand;        


        public enum ProcessingStateEnum
        {
            Idle = 0,
            ViewProfile = 1,
            Convert = 2
        };        

        #region Constructors

        public TDLConververVM(IDialogService dialogService)
        {            
            DialogService = dialogService;
            TDLConverterCore.ProgressUpdate += OnProgressUpdate;
            TDLConverterCore.ProcessCompleted += OnProcessCompleted;
        }        

        #endregion Constructors

        #region Properties

        public IDialogService DialogService { get; set; }       
        
        public string UserEnvDataFile
        {
            get
            {
                return TDLConverterCore.UserEnvDataFile;
            }
            set
            {
                if (value != TDLConverterCore.UserEnvDataFile)
                {
                    TDLConverterCore.UserEnvDataFile = value;
                    RaisePropertyChanged(() => UserEnvDataFile);                   
                }                
            }
        }

        public string CompatibleStudioVersion
        {
            get
            {
                return TDLConverterCore.CompatibleStudioVersion;
            }
            set
            {
                if (value != TDLConverterCore.CompatibleStudioVersion)
                {
                    TDLConverterCore.CompatibleStudioVersion = value;
                    RaisePropertyChanged(() => CompatibleStudioVersion);
                }
            }
        }        

        public bool AutoFillOutputFolder 
        {
            get 
            {
                return m_AutoFillOutputFolder;
            }
            set
            {
                if (m_AutoFillOutputFolder != value)
                {
                    m_AutoFillOutputFolder = value;

                    if (m_AutoFillOutputFolder == true)
                    {
                        OutputPath = JobPath;
                    }                    
                }
            }
        }

        public string MainTitle
        {
            get
            {
                return string.Format(Strings.MainTitle, Assembly.GetEntryAssembly().GetName().Version, RetrieveLinkerTimestamp());
            }
        }

        public BearingMaterialEnum BearingMaterial
        {
            get
            {
                return TDLConverterCore.BearingMaterial;
            }
            set
            {
                if (value != TDLConverterCore.BearingMaterial)
                {
                    TDLConverterCore.BearingMaterial = value;
                    RaisePropertyChanged(() => BearingMaterial);
                }
            }
        }

        public ProcessingStateEnum ProcessingState 
        { 
            get
            {
                return m_ProcessingState;
            }
            set
            {
                if (m_ProcessingState != value)
                {
                    m_ProcessingState = value;
                    RaisePropertyChanged(() => ProcessingStateIdle);
                }
            }
        }

        public ObservableCollection<ITDLObject> ViewObjects
        {
            get
            {
                if (m_ViewObjects == null)
                {
                    m_ViewObjects = new ObservableCollection<ITDLObject>();
                }

                return m_ViewObjects;
            }
        }
        
        public List<ITDLObject> ProcessedObjects
        {
            get 
            {
                if (m_ProcessedObjects == null)
                {
                    m_ProcessedObjects = new List<ITDLObject>();
                }

                return m_ProcessedObjects;
            }
        }        

        public IConverter TDLConverterCore
        {
            get
            {
                if (m_ConverterCore == null)
                {
                    m_ConverterCore = DILocator.Instance.ConverterCore;

                    if (m_ConverterCore == null)
                    {
                        Log.Error("Failed to get converter core. Application exited");
                        Environment.Exit(-1);
                    }

                }

                return m_ConverterCore;
            }
        }

        public ParseItemVM RootNode
        {
            get
            {
                if (m_RootNode == null)
                {
                    m_RootNode = new ParseItemVM(DILocator.Instance.CreateNewContextObject("Root")) { IsChecked = false };
                }
                return m_RootNode;
            }
        }      

        public string JobPath 
        { 
            get
            {
                return m_JobPath;
            }
            set
            {
                m_JobPath = value;
                RaisePropertyChanged(() => JobPath);

                if (AutoFillOutputFolder)
                {
                    OutputPath = m_JobPath;
                }
            }
        }

        public string OutputPath 
        { 
            get
            {
                return m_OutputPath;
            }
            set
            {
                m_OutputPath = value;
                RaisePropertyChanged(() => OutputPath);
            }
        }
        
        public int ProgressPercentage
        {
            get
            {
                return m_ProgressPercentage;
            }
            set
            {
                if (m_ProgressPercentage != value)
                {
                    m_ProgressPercentage = value;                   

                    RaisePropertyChanged(() => ProgressPercentage);
                    RaisePropertyChanged(() => LabelPercentage);                    
                }
            }
        }

        public string LabelPercentage
        {
            get
            {
                return string.Format(Strings.Label_Percentage, m_ProgressPercentage);
            }
        }

        public bool ShowWaitingLabel
        {
            get
            {
                return m_ShowWaitingLabel;
            }
            set
            {
                if (m_ShowWaitingLabel != value)
                {
                    m_ShowWaitingLabel = value;
                    RaisePropertyChanged(() => ShowWaitingLabel);
                }                
            }
        }

        public string OutPutTextBlock
        {
            get
            {
                if (m_ConsoleOutPut == null)
                {
                    m_ConsoleOutPut = string.Empty;
                }

                return m_ConsoleOutPut;
            }
            set
            {
                m_ConsoleOutPut = value;
                RaisePropertyChanged(() => OutPutTextBlock);
            }
        }

        public bool ProcessingStateIdle
        { 
            get
            {
                return ProcessingStateEnum.Idle == ProcessingState;
            }
        }

        #endregion Properties

        #region Commands

        public ICommand ClosingCommand
        {
            get
            {
                return m_ClosingCommand ?? (m_ClosingCommand = new SimpsonRelayCommand((p) => OnClosing()));
            }
        }

        private void OnClosing()
        {
            if (!ProcessingStateIdle)
            {
                MessageBox.Show("Wait for end of process", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            TDLConverterCore.SaveUserSettings();
        }

        public ICommand StopProcessCommand
        { 
            get
            {
                return m_StopProcessCommand ?? (m_StopProcessCommand = new SimpsonRelayCommand((p) => StopProcess()));
            }
        }

        private void StopProcess()
        {
            TDLConverterCore.StopProcess();
        }

        public ICommand CopyAllConsoleOutputCommand
        {
            get
            {
                return m_CopyAllConsoleOutputCommand ?? (m_CopyAllConsoleOutputCommand = new SimpsonRelayCommand((p) => { Clipboard.SetText(OutPutTextBlock); },
                                                                                                                 (p) => CanExecuteCopyAll));
            }
        }

        private bool CanExecuteCopyAll
        {
            get 
            {
                return !string.IsNullOrWhiteSpace(OutPutTextBlock);
            }
        }

        public ICommand ClearAllConsoleOutputCommand
        {
            get
            {
                return m_ClearAllConsoleOutputCommand ?? (m_ClearAllConsoleOutputCommand = new SimpsonRelayCommand((p) => { OutPutTextBlock = string.Empty; },
                                                                                                                   (p) => CanExecuteClearAll));
            }
        }

        private bool CanExecuteClearAll
        {
            get
            {
                return !string.IsNullOrWhiteSpace(OutPutTextBlock);
            }
        }

        public ICommand InputFolderCommand
        { 
            get
            {
                return m_InputFolderCommnad ?? (m_InputFolderCommnad = new SimpsonRelayCommand((p) => SelectJobFolder(),
                                                                                               (p) => ProcessingStateIdle));
            }            
        }

        private void SelectJobFolder()
        {
            var ofd = new OpenFileDialog()
            {
                Multiselect = false,
                CheckPathExists = true,
                Title = "Select Job Folder",
                ValidateNames = false,
                CheckFileExists = false,
                FileName = "Folder Selection.",
                InitialDirectory = JobPath
            };

            if (ofd.ShowDialog() == true)
            {
                var dirName = new DirectoryInfo(Path.GetDirectoryName(ofd.FileName));

                JobPath = dirName.FullName;

                RootNode.SubNodes.Clear();
                ViewObjects.Clear();
                ProcessedObjects.Clear();
                TDLConverterCore.OutputTDLObjects.Clear();
                DILocator.Instance.ClearAllContextObjects();

                OutPutTextBlock = string.Empty;
                RootNode.IsChecked = false;

                foreach (var file in dirName.GetFiles(Util.MitekExtension).OrderBy(x => x.Name))
                {
                    RootNode.SubNodes.Add(new ParseItemVM(DILocator.Instance.CreateNewContextObject(file.FullName)) { ParentItem = RootNode, IsChecked = false });
                }
            }
        }

        public ICommand OutputFolderCommand
        {
            get
            {
                return m_OutputFolderCommnad ?? (m_OutputFolderCommnad = new SimpsonRelayCommand((p) => SelectOutputFolder(),
                                                                                                 (p) => CanExecuteSelectOutputFolder));
            }
        }

        private void SelectOutputFolder()
        {
            var ofd = new OpenFileDialog()
            {
                Multiselect = false,
                CheckPathExists = true,
                Title = "Select Output Folder",
                ValidateNames = false,
                CheckFileExists = false,
                FileName = "Folder Selection.",
                InitialDirectory = OutputPath
            };

            if (ofd.ShowDialog() == true)
            {
                var dirName = new DirectoryInfo(Path.GetDirectoryName(ofd.FileName));
                OutputPath = dirName.FullName;
            }
        }

        private bool CanExecuteSelectOutputFolder
        {
            get
            {
                if (!ProcessingStateIdle)
                {
                    return false;
                }

                if (AutoFillOutputFolder)
                {
                    return false;
                }

                return true;
            }
        }

        private ICommand m_ChooseCompatibleStudioVersionCommand;
        public ICommand ChooseCompatibleStudioVersionCommand
        {
            get 
            {
                return m_ChooseCompatibleStudioVersionCommand ?? (m_ChooseCompatibleStudioVersionCommand = new SimpsonRelayCommand((p) => ChooseStudioVersion(),
                                                                                                                                   (p) => ProcessingStateIdle));
            }
        }

        private void ChooseStudioVersion()
        {
            var ofd = new OpenFileDialog()
            {
                Multiselect = false,
                CheckPathExists = true,
                Title = @"Select target TrussStudio.exe",
                Filter = @"Exe files (*.exe)|*.exe",
                InitialDirectory = @"C:\"
            };

            if (ofd.ShowDialog() == true)
            {
                CompatibleStudioVersion = Util.LoadAssembly(ofd.FileName);
            }
        }

        private ICommand m_LoadUserEnvDataCommand;
        public ICommand LoadUserEnvDataCommand
        {
            get
            {
                return m_LoadUserEnvDataCommand ?? (m_LoadUserEnvDataCommand = new SimpsonRelayCommand((p) => LoadUserEnvData(), (p) => ProcessingStateIdle));
            }            
        }

        private void LoadUserEnvData()
        {
            var ofd = new OpenFileDialog()
            {
                Multiselect = false,
                CheckPathExists = true,
                Title = @"Select target .tdlEnv file",
                Filter = @"Env (*.tdlEnv)|*.tdlEnv",
                InitialDirectory = Path.GetDirectoryName(UserEnvDataFile) ?? @"C:\"
            };

            if (ofd.ShowDialog() == true)
            {
                UserEnvDataFile = ofd.FileName;                
            }

            CheckEnvDataVersion();
        }        

        public ICommand ViewProfileCommand
        {
            get
            {
                return m_ViewProfileCommand ?? (m_ViewProfileCommand = new SimpsonRelayCommand((p) => ViewProfile(), 
                                                                                               (p) => CanExecuteViewProfile));
            }
        }

        public bool CanExecuteViewProfile
        {
            get
            {
                if (!ProcessingStateIdle)
                {
                    return false;
                }

                if (!RootNode.SubNodes.Where(node => node.IsChecked.GetValueOrDefault()).Any())
                {
                    return false;
                }

                if (string.IsNullOrWhiteSpace(CompatibleStudioVersion) || new StudioVersion(CompatibleStudioVersion).IsEmpty)
                {
                    return false;
                }

                if (string.IsNullOrWhiteSpace(UserEnvDataFile))
                {
                    return false;
                }

                return true;
            }            
        }

        private void ViewProfile()
        {
            if (!CheckEnvDataVersion())
            {
                return;
            }

            ShowWaitingLabel = true;
            ProcessingState = ProcessingStateEnum.ViewProfile;
            OutPutTextBlock = string.Empty;
            ViewObjects.Clear();

            m_ItemsToProcess = RootNode.SubNodes.Where(node => node.IsChecked.GetValueOrDefault()).Select(item => item.Model as IContextObject).ToList();
            if (m_ItemsToProcess.Any())
            {
                m_Stopwatch.Restart();
                TDLConverterCore.DoParse(m_ItemsToProcess);
            }
        }        

        public ICommand ConvertCommand
        {
            get
            {
                return m_ConvertCommand ?? (m_ConvertCommand = new SimpsonRelayCommand((p) => ConvertToTDL(),
                                                                                       (p) => CanExecuteConvert));
            }
        }

        private void ConvertToTDL()
        {
            if (!CheckEnvDataVersion())
            {
                return;
            }

            ShowWaitingLabel = true;
            ProcessingState = ProcessingStateEnum.Convert;
            OutPutTextBlock = string.Empty;
            ProcessedObjects.Clear();

            m_ItemsToProcess = RootNode.SubNodes.Where(node => node.IsChecked.GetValueOrDefault()).Select(item => item.Model as IContextObject).ToList();

            if (m_ItemsToProcess.Any())
            {
                m_Stopwatch.Restart();
                TDLConverterCore.DoConvert(m_ItemsToProcess, OutputPath);
            }                  
        }

        public bool CanExecuteConvert
        {
            get
            {
                if (!ProcessingStateIdle)
                {
                    return false;
                }

                if (!RootNode.SubNodes.Where(node => node.IsChecked.GetValueOrDefault()).Any())
                {
                    return false;
                }

                if (string.IsNullOrWhiteSpace(OutputPath))
                {
                    return false;
                }

                if (string.IsNullOrWhiteSpace(CompatibleStudioVersion) || new StudioVersion(CompatibleStudioVersion).IsEmpty)
                {
                    return false;
                }

                if (string.IsNullOrWhiteSpace(UserEnvDataFile))
                {
                    return false;
                }

                return true;
            }
        }

        public ICommand ExportReportCommand
        {
            get
            {
                return m_ExportReportCommand ?? (m_ExportReportCommand = new SimpsonRelayCommand((p) => ExportReport(), (p) => CanExecuteReport));
            }
        }

        private bool CanExecuteReport
        {
            get
            {
                if (!ProcessingStateIdle)
                {
                    return false;
                }

                if (!ProcessedObjects.Any())
                {
                    return false;
                }

                return true;   
            }            
        }

        private void ExportReport()
        {
            var reportName = DialogService.DisplayReportNameDialog();

            if (reportName == null)
            {
                return;
            }

            using (new WaitCursor())
            {
                try
                {
                    reportName = string.Format("{0}{1}", Path.GetFileNameWithoutExtension(reportName), @".xlsm");
                    var appPath = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                    var templatePath = Path.Combine(appPath, "Template.xlsm");

                    int passedNumber = m_ItemsToProcess.Where(item => !(item.IsProcessFailed || item.IsRejected)).Count();
                    int failedNumner = m_ItemsToProcess.Where(item => item.IsProcessFailed).Count();
                    int rejectedNumber = m_ItemsToProcess.Where(item => item.IsRejected).Count();

                    int startRow = 11; // Start writing at row 11th as Template.xlsm defined

                    var excelExporter = new ExcelExporter(templatePath, m_ItemsToProcess[0].Extension, m_ItemsToProcess.Count, passedNumber, failedNumner, rejectedNumber, Path.GetFileName(JobPath));

                    foreach (var obj in ProcessedObjects)
                    {
                        var contextItem = m_ItemsToProcess.FirstOrDefault(item => item.FileNameNoExtension.Equals(obj.Title, StringComparison.OrdinalIgnoreCase));

                        var expInfo = new string[12];

                        expInfo[0] = obj.Title;
                        expInfo[1] = obj.Quantity.ToString();
                        expInfo[2] = obj.Ply.ToString();
                        expInfo[3] = Util.FormatFIS(obj.Span);
                        expInfo[4] = Util.FormatFIS(obj.Spacing);
                        expInfo[5] = Util.FormatFIS(obj.Left_OH);
                        expInfo[6] = Util.FormatFIS(obj.Right_OH);
                        expInfo[7] = Util.FormatFIS(obj.Left_HeelHeight);
                        expInfo[8] = Util.FormatFIS(obj.Right_HeelHeight);
                        expInfo[9] = Util.FormatFIS(obj.OverallHeight);
                        expInfo[10] = obj.TrussType;
                        expInfo[11] = (contextItem == null) ? string.Empty : contextItem.IsProcessFailed ? "Convert Failed" :
                                        contextItem.IsRejected ? string.Format("Rejected, {0}", contextItem.CauseOfReject.ToString()) : "Convert Success";

                        excelExporter.WriteExcell(startRow, expInfo);

                        startRow++;
                    }

                    excelExporter.SaveAndOpenReport(Path.Combine(OutputPath, reportName), startRow - 1);
                }
                catch (Exception ex)
                {
                    Log.Error("Failed to export Report", ex);
                }
            }
        }

        #endregion Commnands

        #region Methods

        private bool CheckEnvDataVersion()
        {
            if (string.IsNullOrWhiteSpace(UserEnvDataFile))
            {
                return false;
            }

            var path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Env\\EnvData_defaultValue.tdlEnv");
            var localEnvDataDoc = XDocument.Load(path);        
            var localEnvDatatVersion = localEnvDataDoc.Root.Attributes().First(Attribute => Attribute.Name == "Version").Value;

            var userEnvDataDoc = XDocument.Load(UserEnvDataFile);
            var userEnvDataVersion = userEnvDataDoc.Root.Attributes().First(Attribute => Attribute.Name == "Version").Value;

            // Just need to compare first three (major, minor and build) numbers of the version strings
            var localVersion = new StudioVersion(string.Join(".", localEnvDatatVersion.Split('.')).Trim());
            var userVersion = new StudioVersion(string.Join(".", userEnvDataVersion.Split('.')).Trim());

            if (localVersion < userVersion)
            {
                MessageBox.Show(string.Format(Strings.VersionMismatchWarning, Path.GetFileName(path), Path.GetDirectoryName(path)), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            return true;
        }

        public override void CanClose(Action<bool> callback)
        {
            callback(ProcessingStateIdle);
        }        

        private void OnProgressUpdate(int percentage, ProcessState state, IContextObject passedContextObject, ITDLObject passedTdlObject)
        {
            // Update percentage
            ProgressPercentage = percentage;

            // Update Truss Info grid
            if (ProcessingState == ProcessingStateEnum.ViewProfile)
            {
                ViewObjects.Add(passedTdlObject);
            }

            // Update Output window
            if (!passedContextObject.IsRejected)
            {
                OutPutTextBlock = string.Format("{0}{1} {2}: {3} \"{4}\" {5}.", OutPutTextBlock, string.IsNullOrWhiteSpace(OutPutTextBlock) ? string.Empty : Environment.NewLine, GetElapsedTime(), state == ProcessState.Parse ? "Parse" : "Convert",                                          passedContextObject.FilePath, passedContextObject.IsProcessFailed ? "Failed" : "Done");
            }
            else
            {
                OutPutTextBlock = string.Format("{0}{1} {2}: Item \"{3}\" is Rejected - {4}", OutPutTextBlock, string.IsNullOrWhiteSpace(OutPutTextBlock) ? string.Empty : Environment.NewLine, GetElapsedTime(),                                   passedContextObject.FilePath, passedContextObject.CauseOfReject == RejectCause.CAUSE_UNSUPPORT_TRUSSTYPE ? "Truss type Unsupport" : 
                                        passedContextObject.CauseOfReject == RejectCause.CAUSE_UNIT_INVALID ? "Unit Invalid" :
                                            passedContextObject.CauseOfReject == RejectCause.CAUSE_UNSUPPORT_VERSION ? string.Format("File Version ( {0} ) Unsupport", passedContextObject.FileVersion) : "Unknow");
            }
        }

        private void OnProcessCompleted(bool isCancelled)
        {
            m_Stopwatch.Stop();

            if (!isCancelled)
            {
                if (ProcessingState == ProcessingStateEnum.Convert)
                {
                    ProcessedObjects.Clear();
                    ProcessedObjects.AddRange(TDLConverterCore.OutputTDLObjects);
                }
            }
            else
            {
                OutPutTextBlock = string.Format("{0}{1} {2}: Process has been cancelled.", OutPutTextBlock, string.IsNullOrWhiteSpace(OutPutTextBlock) ? string.Empty : Environment.NewLine, GetElapsedTime());
            }

            ShowWaitingLabel = false;
            ProgressPercentage = 0;
            ProcessingState = ProcessingStateEnum.Idle;

            // Forcing the CommandManager to raise the RequerySuggested event
            CommandManager.InvalidateRequerySuggested();
        }

        /// <summary>
        /// Get elapsed time in millisecond
        /// </summary>
        /// <returns></returns>
        private string GetElapsedTime()
        {
            return string.Format("{0:hh\\:mm\\:ss\\:fff}", m_Stopwatch.Elapsed);
        }

        public DateTime RetrieveLinkerTimestamp()
        {
            string filePath = Assembly.GetCallingAssembly().Location;
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;
            byte[] b = new byte[2048];

            try
            {
                using (var s = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    s.Read(b, 0, 2048);
                }
            }
            catch
            {
                //TODO: handle the exception
            }

            int i = BitConverter.ToInt32(b, c_PeHeaderOffset);
            int secondsSince1970 = BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0);
            dt = dt.AddSeconds(secondsSince1970);
            return TimeZoneInfo.ConvertTimeFromUtc(dt, TimeZoneInfo.Local);
        }

        #endregion Methods      
    }   
}
