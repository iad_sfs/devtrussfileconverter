﻿using devConversionTool.Infrastructure;

namespace devConversionTool.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ApplicationContainer : CSDialog
    {
        public ApplicationContainer()
        {
            InitializeComponent();
        }
    }
}
