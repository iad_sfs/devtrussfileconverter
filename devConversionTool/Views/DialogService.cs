﻿
using devConversionTool.Views.Dialogs;
using Interfaces;
using System.Windows;
namespace devConversionTool.Views
{
    public class DialogService : IDialogService
    {
        /// <summary>
        /// Display Report Name Dialog
        /// </summary>
        /// <returns>Null if canceled. Otherwise, Report Name string</returns>
        public string DisplayReportNameDialog()
        {
            var vm = new ExportReportVM();

            var dialog = new ReportNameDialog()
            {
                DataContext = vm,
                Owner = Application.Current.MainWindow,
            };

            return dialog.ShowDialog().GetValueOrDefault() == true ? vm.ReportName : null;
        }
    }
}
