﻿using devConversionTool.Infrastructure;

namespace devConversionTool.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for ReportNameDialog.xaml
    /// </summary>
    public partial class ReportNameDialog : CSDialog
    {
        public ReportNameDialog()
        {
            InitializeComponent();
        }
    }
}
