﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace devConversionTool
{
    public class ExcelExporter
    {
        #region Members

        private readonly Application m_ExcelApp;
        private readonly Workbook m_ExcelWb;

        #endregion Members

        #region Constructors

        public ExcelExporter(string templatePath, string originalFileType, int itemTotal, int itemPassed, int itemFailed, int itemRejected, string jobFolder)
        {
            TemplatePath = templatePath;

            try
            {
                m_ExcelApp = new Application();
                m_ExcelWb = m_ExcelApp.Workbooks.Open(TemplatePath);
                m_ExcelApp.DisplayAlerts = false;
                m_ExcelApp.Visible = false;
                _Worksheet xlSS = m_ExcelWb.Sheets["Report"];
                xlSS.Activate();
                xlSS.Cells[1, 7] = jobFolder;
                xlSS.Cells[2, 7] = originalFileType;
                xlSS.Cells[4, 7] = itemTotal;
                xlSS.Cells[5, 7] = itemPassed;
                xlSS.Cells[6, 7] = itemFailed;
                xlSS.Cells[7, 7] = itemRejected;
            }
            catch (Exception ex)
            {
                m_ExcelApp.Quit();
                Marshal.ReleaseComObject(m_ExcelApp);
                GC.Collect();
                throw ex;
            }
        }        

        #endregion Constructors

        #region Properties

        public string TemplatePath { get ; set; }

        #endregion Properties

        #region Methods

        public void WriteExcell(int row, string[] expInfo)
        {
            _Worksheet xlWs = m_ExcelWb.Sheets["Report"];
            xlWs.Activate();
            xlWs.Cells[row, 1] = "'" + expInfo[0];
            xlWs.Cells[row, 2] = "'" + expInfo[1];
            xlWs.Cells[row, 3] = "'" + expInfo[2];
            xlWs.Cells[row, 4] = "'" + expInfo[3];
            xlWs.Cells[row, 5] = "'" + expInfo[4];
            xlWs.Cells[row, 6] = "'" + expInfo[5];
            xlWs.Cells[row, 7] = "'" + expInfo[6];
            xlWs.Cells[row, 8] = "'" + expInfo[7];
            xlWs.Cells[row, 9] = "'" + expInfo[8];
            xlWs.Cells[row, 10] = "'" + expInfo[9];
            xlWs.Cells[row, 11] = "'" + expInfo[10];
            xlWs.Cells[row, 12] = "'" + expInfo[11];
        }

        public void SaveAndOpenReport(string path, int iRow)
        {
            try
            {
                // Save report sheet
                _Worksheet xlWs = m_ExcelWb.Sheets["Report"];
                xlWs.Activate();
                Range fmtRange = xlWs.get_Range("A10:L" + iRow);
                xlWs.ListObjects.AddEx(XlListObjectSourceType.xlSrcRange, fmtRange, Microsoft.Office.Interop.Excel.XlYesNoGuess.xlYes).Name = "ReportStyle";
                xlWs.ListObjects.get_Item("ReportStyle").TableStyle = "TableStyleMedium4";
                m_ExcelWb.Close(true, path, Missing.Value);

                // Open for user to view
                m_ExcelApp.Workbooks.Open(path);
                m_ExcelApp.DisplayAlerts = false;
                m_ExcelApp.Visible = true;
            }
            catch (Exception ex)
            {
                m_ExcelApp.Quit();
                Marshal.ReleaseComObject(m_ExcelApp);
                GC.Collect();
                throw ex;
            }
        }

        #endregion Methods
    }
}
