﻿using devConversionTool.ViewModels;
using devConversionTool.Views;
using System.Windows;

namespace devConversionTool
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            var mainWindow = new ApplicationContainer();
            mainWindow.DataContext = ViewModelLocator.Instance.MainViewModel;
            mainWindow.Show();
        }
    }
}
