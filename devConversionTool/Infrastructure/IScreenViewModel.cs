﻿using System;

namespace devConversionTool.Infrastructure
{
    public interface IScreenViewModel
    {
        /// <summary>
        /// Placeholder for a hook in corresponding view, used to close the view.
        /// </summary>
        Action<bool?> CloseView { get; set; }

        /// <summary>
        /// Called to check whether or not this instance can close.
        /// </summary>
        /// <param name="callback">The implementer calls this action with the result of the close check.</param>
        void CanClose(Action<bool> callback);

        /// <summary>
        /// Tries to accept changes.
        /// </summary>
        void Ok();

        /// <summary>
        /// Specifies whether changes can be accepted.
        /// </summary>
        bool CanOk { get; }

        /// <summary>
        /// Skips all changes and close the corresponding view.
        /// </summary>
        void Cancel();
    }
}
