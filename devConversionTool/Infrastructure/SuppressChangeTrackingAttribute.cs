﻿using System;

namespace devConversionTool.Infrastructure
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate | AttributeTargets.Property)]
    public sealed class SuppressChangeTrackingAttribute : Attribute
    {
    }
}
