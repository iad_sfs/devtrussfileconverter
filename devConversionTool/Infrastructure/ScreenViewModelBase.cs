﻿using GalaSoft.MvvmLight;
using System;
using System.ComponentModel;
using System.Reflection;

namespace devConversionTool.Infrastructure
{
    public class ScreenViewModelBase : ViewModelBase, IScreenViewModel, IDataErrorInfo
    {
        #region Members

        private bool m_IsDirty;
        private bool m_SuppressValidation;
        private bool? m_DesiredDialogResult = null;

        #endregion Members

        #region Constructors

        protected ScreenViewModelBase()
        {
            PropertyChanged += OnPropertyChanged;
        }

        #endregion Constructors

        #region Properties

        [SuppressChangeTracking]
        public bool IsValidationEnabled { get; set; }

        [SuppressChangeTracking]
        public bool IsDirty
        {
            get { return m_IsDirty; }
            set
            {
                if (m_IsDirty != value)
                {
                    m_IsDirty = value;
                    RaisePropertyChanged(() => IsDirty);
                }
            }
        }

        [SuppressChangeTracking]
        public Action<bool?> CloseView { get; set; }

        [SuppressChangeTracking]
        public virtual bool CanOk
        {
            get { return true; }
        }

        [SuppressChangeTracking]
        protected bool PromptForSavingChangesOnCancel { get; set; }

        [SuppressChangeTracking]
        private bool IsChangeTrackingSuspended { get; set; }

        #endregion Properties

        #region Methods

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateChangeStateOnPropertyChanged(sender, e);
        }

        protected void UpdateChangeStateOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!IsChangeTrackingSuspended && !string.IsNullOrEmpty(e.PropertyName))
            {
                PropertyInfo propertyInfo = sender.GetType().GetProperty(e.PropertyName);

                if (propertyInfo != null && !Attribute.IsDefined(propertyInfo, typeof(SuppressChangeTrackingAttribute)))
                {
                    IsDirty = true;
                }
            }
        }

        public virtual void CanClose(Action<bool> callback)
        {
            var canClose = Validate();

            if (m_DesiredDialogResult.GetValueOrDefault() && canClose)
            {
                AcceptChanges();
            }

            m_DesiredDialogResult = null;
            callback(canClose);
        }

        public virtual void Ok()
        {
            TryClose(false, true);
        }

        public virtual void Cancel()
        {
            RevertChanges();
            TryClose(true, false);
        }

        protected virtual void AcceptChanges()
        {
            IsDirty = false;
        }

        protected void TryClose(bool suppressValidation, bool? dialogResult = null)
        {
            if (CloseView == null)
            {
                return;
            }

            m_DesiredDialogResult = dialogResult;
            m_SuppressValidation = suppressValidation;
            CloseView(dialogResult);
        }

        private bool Validate()
        {
            if (m_SuppressValidation || !m_DesiredDialogResult.HasValue)
            {
                m_SuppressValidation = false;
                return true;
            }

            return ValidateModifications();
        }

        protected virtual bool ValidateModifications()
        {
            /****************************************************************************/
            //// Derived classes may override this method like this:
            //if (!IsChanged)
            //{
            //    return true;
            //}

            //if (PromptForSavingChangesOnCancel)
            //{
            //    System.Windows.MessageBoxResult shouldSave = [PromptForSaving]; // Yes/No/Cancel                
            //    if(shouldSave == System.Windows.MessageBoxResult.Yes)
            //    {
            //        bool saveDataResult = SaveData();
            //        return saveDataResult;
            //    }
            //    else if (shouldSave == System.Windows.MessageBoxResult.No)
            //    {
            //        return true;
            //    }
            //    else if (shouldSave == System.Windows.MessageBoxResult.Cancel)
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            /****************************************************************************/

            // Most of the time, we must make sure that, in derived class, changes to properties that do not relate to model data
            // should not cause dirty state (e.g use [SuppressChangeTracking] attribute)
            if (!IsDirty)
            {
                return true;
            }

            return !PromptForSavingChangesOnCancel;
        }

        protected virtual void RevertChanges()
        {            
        }       

        protected void SuspendChangeTracking()
        {
            IsChangeTrackingSuspended = true;
        }

        protected void ResumeChangeTracking()
        {
            IsChangeTrackingSuspended = false;
        }
        #endregion Methods

        public string Error
        {
            get 
            {
                return null;
            }
        }

        public string this[string columnName]
        {
            get 
            {
                if (!IsValidationEnabled)
                {
                    return null;
                }

                return ValidateProperty(columnName);
            }
        }

        protected virtual string ValidateProperty(string propertyname)
        {
            // let's derived classes implement this

            return null;
        }
    }

    public class ScreenViewModelBase<T> : ScreenViewModelBase
    {
        private T m_Model;

        public T Model
        {
            get
            {
                return m_Model;
            }
            set
            {
                if (Equals(m_Model, value))
                {
                    return;
                }

                m_Model = value;
                RaisePropertyChanged(() => Model);
            }
        }
    }
}