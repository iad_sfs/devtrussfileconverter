﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;

namespace devConversionTool.Infrastructure
{
    public class CSDialog : Window
    {
        #region Members

        private Button m_DefaultButton;
        private Button m_CancelButton;

        #endregion Members

        #region Constructors

        public CSDialog()
            : base()
        {
            Loaded += OnLoaded;
            Closed += OnClosed;
            Closing += OnClosing;
            DataContextChanged += OnDataContextChanged;
        }        

        #endregion Constructors

        #region Properties

        private bool IsDialog
        {
            get
            {
                return ComponentDispatcher.IsThreadModal;
            }
        }

        private IScreenViewModel ViewModel
        {
            get
            {
                return DataContext as IScreenViewModel;
            }
        }

        #endregion Properties

        #region Methods

        private void OnClosed(object sender, EventArgs e)
        {
            Loaded -= OnLoaded;
            Closed -= OnClosed;

            DataContextChanged -= OnDataContextChanged;

            if (m_DefaultButton != null)
            {
                m_DefaultButton.Command = null;
            }

            if (m_CancelButton != null)
            {
                m_CancelButton.Command = null;
            }

            if (ViewModel != null)
            {
                ViewModel.CloseView = null;
            }
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            if (e.Cancel)
            {
                return;
            }
            
            var viewModel = ViewModel;

            if (viewModel == null)
            {
                return;
            }

            viewModel.CanClose(canClose =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    e.Cancel = !canClose;
                });
            });
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            IScreenViewModel viewModel = e.OldValue as IScreenViewModel;
            if (viewModel != null)
            {
                viewModel.CloseView = null;
            }

            viewModel = e.NewValue as IScreenViewModel;
            if (viewModel != null)
            {
                viewModel.CloseView = TryClose;
            }

            ConfigureDefaultButtons();
        }        

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            m_DefaultButton = this.FindVisualChild<Button>(button => button.IsDefault);
            m_CancelButton = this.FindVisualChild<Button>(button => button.IsCancel);

            ConfigureDefaultButtons();
        }

        private void TryClose(bool? dialogResult = null)
        {
            if (IsDialog && dialogResult.HasValue)
            {
                DialogResult = dialogResult;
            }
            else
            {
                Close();
            }
        }

        private void ConfigureDefaultButtons()
        {
            if (ViewModel != null)
            {
                if (m_DefaultButton != null && m_DefaultButton.Command == null)
                {
                    m_DefaultButton.Command = new SimpsonRelayCommand(obj => ViewModel.Ok(), obj => ViewModel.CanOk);
                }

                if (m_CancelButton != null && m_CancelButton.Command == null)
                {
                    m_CancelButton.Command = new SimpsonRelayCommand(obj => ViewModel.Cancel());
                }
            }
        }

        #endregion Methods
    }
}