﻿using GalaSoft.MvvmLight;

namespace devConversionTool.Infrastructure
{
    public abstract class ViewModelBase<TModel> : ViewModelBase
    {
        TModel _model;

        public TModel Model
        {
            get
            {
                return _model;
            }
            set
            {
                if (Equals(_model, value))
                {
                    return;
                }

                _model = value;
                RaisePropertyChanged("Model");
            }
        }
    }
}
