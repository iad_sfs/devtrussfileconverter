﻿using System;
using System.Windows;
using System.Windows.Media;

namespace devConversionTool.Infrastructure
{
    public static class VisualExtensions
    {
        public static T FindVisualChild<T>(this DependencyObject parent) where T : DependencyObject
        {
            DependencyObject child = null;
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                child = VisualTreeHelper.GetChild(parent, i) as DependencyObject;
                if (child is T)
                {
                    break;
                }

                if (child != null)
                {
                    child = FindVisualChild<T>(child);
                }
            }

            return child as T;
        }

        public static T FindVisualChild<T>(this DependencyObject parent, Predicate<T> condition) where T : DependencyObject
        {
            DependencyObject child = null;

            var childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            
            for (int i = 0; i < childrenCount; i++)
            {
                child = VisualTreeHelper.GetChild(parent, i) as DependencyObject;
                T foundChild = child as T;
                
                if (foundChild != null && condition(foundChild))
                {
                    return foundChild;
                }

                if (child != null)
                {
                    foundChild = FindVisualChild<T>(child, condition);

                    if (foundChild != null)
                    {
                        return foundChild;
                    }
                }
            }

            return null;
        }
    }
}
