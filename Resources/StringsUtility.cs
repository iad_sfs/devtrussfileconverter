﻿using System;
using System.Reflection;

namespace Resources
{
    /// <summary>
    /// Provides utility methods for working with <see cref="Strings"/>
    /// </summary>
    public static class StringsUtility
    {
        /// <summary>
        /// Gets a resource string from <see cref="Strings"/>
        /// </summary>
        /// <param name="resource">The string resource to get</param>
        /// <returns>The requested string resource</returns>
        /// <exception cref="ArgumentNullException">The resource parameter is null</exception>
        /// <exception cref="ArgumentException">The requested resource doesn't exist on <see cref="Strings"/></exception>
        public static string GetStringResource(string resource)
        {
            if (resource == null)
            {
                throw new ArgumentNullException("resource");
            }

            try
            {
                return (string)typeof(Strings)
                    .GetProperty(resource, BindingFlags.Public | BindingFlags.Static)
                    .GetValue(null);
            }
            catch (Exception ex)
            {
                throw new ArgumentException("the requested resource doesn't exist on CSStrings", ex);
            }
        }
    }
}
