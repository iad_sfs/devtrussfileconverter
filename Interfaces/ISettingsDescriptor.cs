﻿
using System;
using System.Collections.Generic;

namespace Interfaces
{
    public interface ISettingsDescriptor
    {
        #region Properties
        List<Tuple<string, string>> Settings { get; set; }

        #endregion Properties        
    }
}
