﻿
namespace Interfaces
{
    public interface IParser
    {
        ITDLObject Parse(IContextObject item);
    }
}
