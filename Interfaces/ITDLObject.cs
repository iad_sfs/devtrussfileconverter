﻿
using System.Collections.Generic;
namespace Interfaces
{
    public interface ITDLObject
    {
        string Title { get ; set; }
       
        string Type { get; set; }

        string TrussType { get; set; }

        string FileVersion { get; set; }

        string Version { get; set; }

        string CompatibilityVersion { get; set; }

        string Thickness { get; set; }

        int Ply { get; set; }

        int Quantity { get; set; }

        double Span { get; set; }

        double ProductionSpan { get; set; }

        double Spacing { get; set; }

        string TCProfile { get; set; }

        string BCProfile { get; set; }

        string WebProfile { get; set; }

        string Left_Cantilever { get; set; }

        string Right_Cantilever { get; set; }

        string Left_Stub { get; set; }

        string Right_Stub { get; set; }

        double Left_OH { get; set; }

        double Right_OH { get; set; }

        string OHLProfile { get; set; }

        string OHRProfile { get; set; }

        string Left_OHA { get; set; }

        string Right_OHA { get; set; }

        string Left_EndDesc { get; set; }

        string Right_EndDesc { get; set; }

        string Left_EndDescA { get; set; }

        string Right_EndDescA { get; set; }

        string Gable { get; set; }

        string Attic { get; set; }

        List<string> BearingList { get; }

        string LoadTemplate { get; set; }

        string AdditionalCommand { get; set; }

        double Left_HeelHeight { get; set; }

        double Right_HeelHeight { get; set; }

        double OverallHeight { get; set; }

        IScriptDescriptor ScriptDescriptor { get; }

        ISettingsDescriptor SettingstDescriptor { get; set; }

        void WriteTDLFile(string outputPath);
    }
}
