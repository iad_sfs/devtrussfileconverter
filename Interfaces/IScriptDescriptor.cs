﻿
namespace Interfaces
{
    public interface IScriptDescriptor
    {
        #region Properties

        /// <summary>
        /// Thickness
        /// </summary>
        string Thickness { get; set; }

        /// <summary>
        /// Ply: Integer number more than zero
        /// </summary>
        string Ply { get; set; }

        /// <summary>
        /// Quantity: Integer number more than zero
        /// </summary>
        string Quantity { get; set; }

        /// <summary>
        /// Truss Span
        /// </summary>
        string Span { get; set; }

        /// <summary>
        /// Production Span
        /// </summary>
        string ProductionSpan { get; set; }

        /// <summary>
        /// Spacing
        /// </summary>
        string Spacing { get; set; }

        /// <summary>
        /// TC profile
        /// </summary>
        string TCProfile { get; set; }

        /// <summary>
        /// BC profile
        /// </summary>
        string BCProfile { get; set; }

        /// <summary>
        /// WEB profile
        /// </summary>
        string WebProfile { get; set; }

        /// <summary>
        /// Left Cantilever
        /// </summary>
        string Left_Cantilever { get; set; }

        /// <summary>
        /// Right Cantilever
        /// </summary>
        string Right_Cantilever { get; set; }

        /// <summary>
        /// Left Stub
        /// </summary>
        string Left_Stub { get; set; }

        /// <summary>
        /// Right Stub
        /// </summary>
        string Right_Stub { get; set; }

        /// <summary>
        /// TC Left Overhang
        /// </summary>
        string Left_OH { get; set; }

        /// <summary>
        /// TC Right Overhang
        /// </summary>
        string Right_OH { get; set; }

        /// <summary>
        /// TC Left Overhang Additional
        /// </summary>
        string Left_OHA { get; set; }

        /// <summary>
        /// TC Right Overhang Additional
        /// </summary>
        string Right_OHA { get; set; }

        /// <summary>
        /// End description Left
        /// </summary>
        string Left_EndDesc { get; set; }

        /// <summary>
        /// End description Right
        /// </summary>
        string Right_EndDesc { get; set; }

        /// <summary>
        /// End description Left Additional
        /// </summary>
        string Left_EndDescA { get; set; }

        /// <summary>
        /// End description Right Additional
        /// </summary>
        string Right_EndDescA { get; set; }

        string Gable { get; set; }

        string Attic { get; set; }

        /// <summary>
        /// List of string more than or equal two items, each string describes one bearing per the following format:
        ///listStrBrg[i] = brg:ftinsxBrgSize[i] strStartEndLoc[i] ftinsxBrgXDist[i] BotChd Flat strBrgType[i] NoSpec intPSIofWTP[i] Y
        /// </summary>
        string BearingList { get; set; }

        /// <summary>
        /// LoadTemplate
        /// </summary>
        string LoadTemplate { get; set; }

        /// <summary>
        /// Additional Command
        /// </summary>
        string AdditionalCommand { get; set; }

        /// <summary>
        /// Left heel height
        /// </summary>
        string Left_HeelHeight { get; set; }

        /// <summary>
        /// Right heel height
        /// </summary>
        string Right_HeelHeight { get; set; }

        /// <summary>
        /// Overall height
        /// </summary>
        string OverallHeight { get; set; }

        // ...Add more here

        #endregion Properties

        #region Methods

        string ToString();

        #endregion Methods
    }
}
