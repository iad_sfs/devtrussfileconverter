﻿
namespace Interfaces
{
    public interface IDialogService
    {
        /// <summary>
        /// Display Report Name Dialog
        /// </summary>
        /// <returns>Null if canceled. Otherwise, Report Name string</returns>
        string DisplayReportNameDialog();
    }
}
