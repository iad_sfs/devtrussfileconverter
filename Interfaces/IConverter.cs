﻿
using System.Collections.Generic;
namespace Interfaces
{
    public interface IConverter
    {
        BearingMaterialEnum BearingMaterial { get; set; }
        string CompatibleStudioVersion { get; set; }
        ProgressUpdateHandler ProgressUpdate { get; set; }
        ProcessCompletedHandler ProcessCompleted { get; set; }
        List<ITDLObject> OutputTDLObjects { get; }
        string UserEnvDataFile { get; set; }
        void DoParse(List<IContextObject> items);
        void DoConvert(List<IContextObject> items, string destinationPath);
        void StopProcess();
        void SaveUserSettings();
    }
}
