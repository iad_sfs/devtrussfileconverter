﻿
namespace Interfaces
{
    public delegate void ProgressUpdateHandler(int percentage, ProcessState processState, IContextObject contextObject, ITDLObject tdlObject);
    public delegate void ProcessCompletedHandler(bool isCancelled);

    public enum BearingMaterialEnum
    {
        SPF = 0,
        DFL,
        SP,        
        HF
    };

    public enum ProcessState
    {
        Parse = 0,
        Convert,
    };

    public enum RejectCause
    { 
        CAUSE_UNKNOWN = 0,
        CAUSE_UNIT_INVALID,
        CAUSE_UNSUPPORT_TRUSSTYPE,
        CAUSE_UNSUPPORT_VERSION
    }
}
