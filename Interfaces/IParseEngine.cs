﻿
namespace Interfaces
{
    public interface IParseEngine
    {
        bool IsMatch(IContextObject item);
        ITDLObject Parse(IContextObject item);
    }
}
