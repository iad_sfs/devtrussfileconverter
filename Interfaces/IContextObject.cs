﻿
namespace Interfaces
{
    public interface IContextObject
    {
        string FilePath { get; set; }

        string FileNameNoExtension { get; }

        string FileName { get; }

        string DirName { get; }

        string Extension { get; }

        bool IsRejected { get; set; }

        bool IsProcessFailed { get; set; }

        RejectCause CauseOfReject { get; set; }

        string FileVersion { get; set; }
    }
}
