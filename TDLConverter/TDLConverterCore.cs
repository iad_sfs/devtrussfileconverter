﻿using Interfaces;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Windows;

namespace TDLConverter
{
    public class TDLConverterCore : IConverter
    {
        #region Members

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private List<ITDLObject> m_OutputTDLObjects;
        private event ProgressUpdateHandler m_ProgressUpdate;
        private event ProcessCompletedHandler m_ProcessCompleted;
        private BackgroundWorker m_Worker;
        private IParser m_Parser;

        #endregion Members

        #region Constructors

        public TDLConverterCore(IParser parser)
        {
            m_Parser = parser;
            UserSettings.LoadUserSettings();
        }

        #endregion Constructors

        #region Properties

        public BearingMaterialEnum BearingMaterial 
        { 
            get
            {
                return UserSettings.Instance.BearingMaterial;
            }
            set
            {
                if (UserSettings.Instance.BearingMaterial != value)
                {
                    UserSettings.Instance.BearingMaterial = value;
                }                
            }
        }

        public string UserEnvDataFile
        {
            get
            {
                return UserSettings.Instance.UserEnvDataFile;
            }
            set
            {
                if (UserSettings.Instance.UserEnvDataFile != value)
                {
                    UserSettings.Instance.UserEnvDataFile = value;
                }
            }
        }

        public string CompatibleStudioVersion 
        { 
            get
            {
                return UserSettings.Instance.CompatibleStudioVersion;
            }
            set
            {
                if (UserSettings.Instance.CompatibleStudioVersion != value)
                {
                    UserSettings.Instance.CompatibleStudioVersion = value;
                }
            }
        }

        ProgressUpdateHandler IConverter.ProgressUpdate
        {
            get
            {
                return m_ProgressUpdate;
            }
            set
            {
                m_ProgressUpdate = value;
            }
        }


        ProcessCompletedHandler IConverter.ProcessCompleted
        {
            get
            {
                return m_ProcessCompleted;
            }
            set
            {
                m_ProcessCompleted = value;
            }
        }

        public List<ITDLObject> OutputTDLObjects
        {
            get
            {
                if (m_OutputTDLObjects == null)
                {
                    m_OutputTDLObjects = new List<ITDLObject>();
                }

                return m_OutputTDLObjects;
            }
        }

        #endregion Properties

        #region Methods  

        public void DoParse(List<IContextObject> items)
        {
            m_Worker = new BackgroundWorker();
            m_Worker.WorkerReportsProgress = true;
            m_Worker.WorkerSupportsCancellation = true;

            OutputTDLObjects.Clear();
            int count = 0;

            m_Worker.DoWork += (s, e) => {

                BackgroundWorker sendingWorker = s as BackgroundWorker;
                foreach (var item in items)
                {
                    if (sendingWorker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }

                    var localTDL = m_Parser.Parse(item);

                    int percentage = ++count * 100 / items.Count;

                    sendingWorker.ReportProgress(percentage, new Tuple<IContextObject, ITDLObject>(item, localTDL));
                };
            
            };

            m_Worker.RunWorkerCompleted += (s, e) => {
                                
                if (e.Cancelled)
                {
                    MessageBox.Show("Process is cancelled.", "Cancel", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else if (e.Error != null)
                {
                    if (MessageBox.Show("Failure parsing.\n Would you like to view failure diagnostic info?",
                                        "Error", MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.Yes)
                    {
                        MessageBox.Show(e.Error.ToString(), "Failure diagnostic info");
                    }
                }

                BackgroundWorker sendingWorker = s as BackgroundWorker;

                if (m_ProcessCompleted != null)
                {
                    m_ProcessCompleted(e.Cancelled);
                }

                if (sendingWorker != null)
                {
                    sendingWorker.Dispose();
                }
            };

            m_Worker.ProgressChanged += (s, e) => {                

                var tuple = e.UserState as Tuple<IContextObject, ITDLObject>;

                OutputTDLObjects.Add(tuple.Item2);

                if (m_ProgressUpdate != null)
                {                    
                    m_ProgressUpdate(e.ProgressPercentage, ProcessState.Parse, tuple.Item1, tuple.Item2);
                }                
            };

            if (!m_Worker.IsBusy)
            {
                m_Worker.RunWorkerAsync();
            }            
        }

        
        public void DoConvert(List<IContextObject> items, string destinationPath)
        {
            m_Worker = new BackgroundWorker();
            m_Worker.WorkerReportsProgress = true;
            m_Worker.WorkerSupportsCancellation = true;

            OutputTDLObjects.Clear();
            int count = 0;

            m_Worker.DoWork += (s, e) =>
            {
                BackgroundWorker sendingWorker = s as BackgroundWorker;
                foreach (var item in items)
                {
                    if (sendingWorker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }                    

                    var localTDL = m_Parser.Parse(item);

                    if (!item.IsProcessFailed && !item.IsRejected)
                    {
                        localTDL.WriteTDLFile(destinationPath);
                    }                    

                    int percentage = ++count * 100 / items.Count;

                    sendingWorker.ReportProgress(percentage, new Tuple<IContextObject, ITDLObject>(item, localTDL));
                };

            };

            m_Worker.RunWorkerCompleted += (s, e) =>
            {

                if (!e.Cancelled && e.Error == null)
                {
                    MessageBox.Show("Convert complete successfully.", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else if (e.Cancelled)
                {
                    MessageBox.Show("Convert process is cancelled.", "Cancel", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    if (MessageBox.Show("Failure parsing.\n Would you like to view failure diagnostic info?",
                                        "Error", MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.Yes)
                    {
                        MessageBox.Show(e.Error.ToString(), "Failure diagnostic info");
                    }
                }

                BackgroundWorker sendingWorker = s as BackgroundWorker;

                if (m_ProcessCompleted != null)
                {
                    m_ProcessCompleted(e.Cancelled);
                }

                if (sendingWorker != null)
                {
                    sendingWorker.Dispose();
                }
            };

            m_Worker.ProgressChanged += (s, e) =>
            {
                var tuple = e.UserState as Tuple<IContextObject, ITDLObject>;

                OutputTDLObjects.Add(tuple.Item2);

                if (m_ProgressUpdate != null)
                {
                    m_ProgressUpdate(e.ProgressPercentage, ProcessState.Convert, tuple.Item1, tuple.Item2);
                }
            };

            if (!m_Worker.IsBusy)
            {
                m_Worker.RunWorkerAsync();
            }
        }

        public void StopProcess()
        {
            if (m_Worker != null)
            {
                m_Worker.CancelAsync();
            }
        }

        public void SaveUserSettings()
        {
            UserSettings.Instance.Save();
        }

        #endregion Methods      
    }
}
