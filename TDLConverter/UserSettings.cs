﻿
using Interfaces;
using log4net;
using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
namespace TDLConverter
{
    public class UserSettings
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static UserSettings Instance = new UserSettings();

        #region Constructors

        private UserSettings()
        { }

        #endregion Constructors

        public static string UserDataFile
        {
            get
            {
                return Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "ConverterUserSettings.xml");
            }
        }

        #region Settings that get saved

        public bool IsInitialized;
        public BearingMaterialEnum BearingMaterial { get ; set; }
        public string CompatibleStudioVersion { get; set; }

        public string UserEnvDataFile { get; set; }

        #endregion Settings that get saved

        #region Methods

        public static void LoadUserSettings()
        {
            UserSettings loadedSettings;

            try
            {
                if (File.Exists(UserDataFile))
                {
                    var serializer = new XmlSerializer(typeof(UserSettings));
                    using (var reader = new StreamReader(UserDataFile))
                    {
                        loadedSettings = (UserSettings)serializer.Deserialize(reader);
                    }
                }
                else
                {
                    Log.Info("No user settings found - initializing default values");
                    loadedSettings = new UserSettings();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to load user settings - initializing default values", ex);
                loadedSettings = new UserSettings();
            }

            // Setup default values
            if (!loadedSettings.IsInitialized)
            {
                loadedSettings.BearingMaterial = BearingMaterialEnum.SPF;
                loadedSettings.CompatibleStudioVersion = @"0.0.0.0";
                loadedSettings.IsInitialized = true;
            }

            Instance = loadedSettings;
        }

        /// <summary>
        /// Saves all settings to the default location
        /// </summary>
        /// <returns>True if the settings were saved successfully, false otherwise</returns>
        public bool Save()
        {
            try
            {
                var serializer = new XmlSerializer(typeof(UserSettings));
                using (var writer = new StreamWriter(UserDataFile))
                {
                    serializer.Serialize(writer, this);
                }

                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Failed to save user settings", ex);
            }

            return false;
        }

        #endregion Methods        
    }
}
