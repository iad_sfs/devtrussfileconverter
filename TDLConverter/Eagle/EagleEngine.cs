﻿
using Interfaces;
using Models;
namespace TDLConverter.Eagle
{
    public class EagleEngine : IParseEngine
    {
        public bool IsMatch(IContextObject item)
        {
            return false;
        }

        public ITDLObject Parse(IContextObject item)
        {
            item.IsRejected = true;
            item.CauseOfReject = RejectCause.CAUSE_UNKNOWN;
            return new TDLObject() { Title = item.FileNameNoExtension.ToUpper() };
        }
    }
}
