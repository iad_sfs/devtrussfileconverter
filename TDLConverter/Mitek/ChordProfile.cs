﻿
using System;
using Utils;

namespace TDLConverter.Mitek
{
    public class ChordProfile : ILineSegment
    {
        #region Properties

        public Point PointB { get; set; }
        public Point PointE { get; set; }      
        public bool IsFirst { get; set; }
        public bool IsLast { get; set; }
        public string Size { get; set; }

        public bool IsValid
        {
            get
            {
                if (double.IsNaN(Pitch))
                {
                    return false;
                }

                if (Pitch < 0 && Dist < 0)
                {
                    return false;
                }

                if (Dist == 0)
                {
                    return false;
                }

                return true;
            }
        }

        public string PitchInString
        {
            get
            {
                if (Pitch == GlobalHelper.INFINITY || IsVertical)
                {
                    return "v";
                }

                return Pitch.ToString();
            }
        }

        public string DistInString
        {
            get
            {
                if (IsLast)
                {
                    return "e";
                }

                return Util.ToFeetInchSix(Dist);
            }
        }

        public bool IsVertical
        {
            get
            {
                if (Util.Equals(PointB.X, PointE.X))
                {
                    return true;
                }

                return false;
            }
        }

        public double Pitch
        {
            get
            {
                if (IsVertical)
                {
                    return GlobalHelper.INFINITY;
                }
                else
                {
                    return Math.Round((PointE.Y - PointB.Y) / (PointE.X - PointB.X) * 12, 2);
                }
            }
        }

        public double Dist
        {
            get
            {
                if (IsVertical)
                {
                    return (PointE.Y - PointB.Y);
                }
                else
                {
                    return (PointE.X - PointB.X);
                }
            }
        }

        #endregion Properties

        #region Methods

        public void SetBeginEndPoints(Point begin, Point end)
        {
            PointB = begin;
            PointE = end;
        }

        public void SetEndPoint(Point newEnd)
        {
            SetBeginEndPoints(PointB, newEnd);
        }


        public override string ToString()
        {
            if (string.IsNullOrWhiteSpace(Size))
            {
                return string.Join(" ", PitchInString, DistInString);
            }
            else
            {
                if (!IsValid)
                {
                    // The wrong chord, should not exit in the chord command string
                    return string.Empty;
                }

                return string.Join(" ", "c" + Size, PitchInString, DistInString);
            }
        }

        /// <summary>
        /// Check whether 2 chords has end intersection
        /// </summary>
        /// <param name="other">other chord</param>
        /// <returns></returns>
        public bool HasEndIntersect2d(ChordProfile other)
        {
            if ((Util.Equals(other.PointB.X, PointB.X) && Util.Equals(other.PointB.Y, PointB.Y)) || 
                (Util.Equals(other.PointE.X, PointB.X) && Util.Equals(other.PointE.Y, PointB.Y)))
            {
                return true;
            }

            if ((Util.Equals(other.PointB.X, PointE.X) && Util.Equals(other.PointB.Y, PointE.Y)) ||
                (Util.Equals(other.PointE.X, PointE.X) && Util.Equals(other.PointE.Y, PointE.Y)))
            {
                return true;
            }

            return false;
        }

        #endregion Methods
    }
}

