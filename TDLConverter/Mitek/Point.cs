﻿
using System;
using Utils;
namespace TDLConverter.Mitek
{
    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point()
        {
            X = Y = 0.0;
        }

        public Point(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Point p = (Point)obj;
            return this.Equals(p);
        }

        public override int GetHashCode()
        {
            return (int)X ^ (int)Y;
        }

        public bool Equals(Point other)
        {
            if (Util.Equals(this.X, other.X) && Util.Equals(this.Y, other.Y))
            {
                return true;
            }

            return false;
        }



        public double Distance2d(Point other)
        {
            var dist = Math.Sqrt(Math.Pow((X - other.X), 2) + Math.Pow((Y - other.Y), 2));

            return dist;
        }

        public double ShortestDistance2d(ILineSegment line)
        {
            Point closestPointOnChord = getClosestPointOnChord(line);
            return Distance2d(closestPointOnChord);
        }

        private Point getClosestPointOnChord(ILineSegment line)
        {
            double sx1 = line.PointB.X;
            double sy1 = line.PointB.Y;
            double sx2 = line.PointE.X;
            double sy2 = line.PointE.Y;
            double xDelta = sx2 - sx1;
            double yDelta = sy2 - sy1;

            if ((xDelta == 0) && (yDelta == 0))
            {
                throw new ArgumentException("Segment start equals segment end");
            }

            double u = ((X - sx1) * xDelta + (Y - sy1) * yDelta) / (xDelta * xDelta + yDelta * yDelta);

            Point closestPoint;

            if (u < 0)
            {
                closestPoint = new Point(sx1, sy1);
            }
            else if (u > 1)
            {
                closestPoint = new Point(sx2, sy2);
            }
            else
            {
                closestPoint = new Point(Math.Round(sx1 + u * xDelta, 3), Math.Round(sy1 + u * yDelta, 3));
            }

            return closestPoint;
        }
    }
}
