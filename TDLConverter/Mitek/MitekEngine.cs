﻿using Interfaces;
using log4net;
using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Utils;

namespace TDLConverter.Mitek
{
    public class MitekEngine : IParseEngine
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private const string MITEKEXTENSION = @".tre";
        private const string BOTCHD = @"BotChd";
        private const string TOPCHD = @"TopChd";
        private const string NOSPEC = @"NoSpec";
        private const string DROPGABLE = @"Dropp";
        private const int    VERSION_SUPPORT_FROM = 7350;

        private const string IDSTRING_IMPERIALIMPERIAL = @"Imperial Imperial";        
        private const string IDSTRING_BEARINGINFO = @"BEARING INFO";
        private const string IDSTRING_ADDITIONALTRUSSINFO = @"[ADDITIONAL TRUSS INFO]";
        private const string IDSTRING_MEMBERINFO = @"MEMBER INFO";
        private const string IDSTRING_ROOFBASICS = @"ROOF BASICS";
        private const string IDSTRING_FLOORBASICS = @"FLOOR BASICS";

        private List<string> m_ReadLines;
        private List<string> m_AdditionalTrussInfoRange;
        private List<MemberInfo> m_AllMembers;
        private SortedList<int, string> m_IDStringDict;
        private IContextObject m_CurrContextObj;
        private BearingMaterialEnum m_BearingMaterial;       

        private List<ChordProfile> m_TCProfiles;
        private List<ChordProfile> m_BCProfiles;
        
        #region Properties        
        
        public string FileVersionString
        {
            get
            {
                if (m_ReadLines != null && m_ReadLines.Any())
                {
                    return m_ReadLines[1].Split(' ')[2];
                }

                return "0000";
            }
        }

        private bool IsValley
        {
            get
            {
                return GetValue(m_AdditionalTrussInfoRange, "Valley=", 1, '=') == @"YES";
            }
        }

        private bool IsPiggyBack
        {
            get
            {
                return GetValue(m_ReadLines, "Piggyback Settings:", 2, ' ') == @"1";
            }
        }

        #endregion Properties

        #region Methods

        public bool IsMatch(IContextObject item)
        {
            return item.Extension.Equals(MITEKEXTENSION, StringComparison.OrdinalIgnoreCase);
        }

        public ITDLObject Parse(IContextObject item)
        {
            ITDLObject tdlObject = new TDLObject() { Title = item.FileNameNoExtension.ToUpper() };

            m_CurrContextObj = item;
            m_BearingMaterial = UserSettings.Instance.BearingMaterial;

            try
            {
                m_ReadLines = File.ReadAllLines(item.FilePath).ToList();

                m_IDStringDict = BuildIDStringList();

                m_AllMembers = BuildMemberList();

                m_AdditionalTrussInfoRange = GetRange(IDSTRING_ADDITIONALTRUSSINFO);
                
                item.FileVersion = GetVersion();

                var result = ValidateTruss();

                if (result != null)
                {
                    item.CauseOfReject = result.GetValueOrDefault();
                    item.IsRejected = true;
                    return tdlObject;
                }

                m_TCProfiles = BuildTCProfiles();
                m_BCProfiles = BuildBCProfiles();

                tdlObject.FileVersion = @"16";
                tdlObject.Version = UserSettings.Instance.CompatibleStudioVersion;
                tdlObject.CompatibilityVersion = UserSettings.Instance.CompatibleStudioVersion;
                tdlObject.Type = @"Truss";
                tdlObject.TrussType = GetValue(m_AdditionalTrussInfoRange, "TRUSS TYPE=", 1, '=');                         
                tdlObject.Thickness = @"0108";
                tdlObject.Ply = Convert.ToInt16(GetValue(m_AdditionalTrussInfoRange, "Ply=", 1, '='));
                tdlObject.Quantity = ComputeQuantity();
                tdlObject.Span = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Max Span=", 1, '='));
                tdlObject.ProductionSpan = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Max Span=", 1, '='));
                tdlObject.Spacing = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Spacing=", 1, '='));
                tdlObject.TCProfile = GetTCCommand();
                tdlObject.BCProfile = GetBCCommand();
                tdlObject.WebProfile = BuildWebbingCommand();
                tdlObject.Left_Cantilever = @"";
                tdlObject.Right_Cantilever = @"";
                tdlObject.Left_Stub = @"";
                tdlObject.Right_Stub = @"";
                tdlObject.Left_OH = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Left Overhang=", 1, '='));
                tdlObject.Right_OH = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Right Overhang=", 1, '='));
                tdlObject.OHLProfile = GetOverHangLeftRightCommand();
                tdlObject.OHRProfile = GetOverHangLeftRightCommand(false);
                tdlObject.Left_OHA = @"";
                tdlObject.Right_OHA = @"";
                tdlObject.Left_EndDesc = GetEndLeftRightDescriptionCommand(true);
                tdlObject.Right_EndDesc = GetEndLeftRightDescriptionCommand(false);
                tdlObject.Left_EndDescA = @"";
                tdlObject.Right_EndDescA = @"";
                tdlObject.Gable = GetGableCommand();
                tdlObject.Attic = @"";
                tdlObject.BearingList.AddRange(GetBearingCommands());
                tdlObject.LoadTemplate = @"";
                tdlObject.Left_HeelHeight = ComputeLeftHeelHeight();
                tdlObject.Right_HeelHeight = ComputeRightHeelHeight();
                tdlObject.OverallHeight = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Truss Height=", 1, '='));
                tdlObject.SettingstDescriptor = new SettingsDescriptor()
                {
                    Settings = Util.GetTdlEnvSettingMismatches(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Env\\EnvData_defaultValue.tdlEnv"), UserSettings.Instance.UserEnvDataFile)
                };
            }
            catch(Exception ex)
            {
                item.IsProcessFailed = true;
                Log.Error("Failed to parse item " + item.FileName, ex);
            }            

            return tdlObject;
        }

        private List<MemberInfo> BuildMemberList()
        {
            var expressT = @"^\s{0,2}[0-9]{1,3}\s[T]{1}[0-9]{1,2}\s[0-9]{1}";
            var expressB = @"^\s{0,2}[0-9]{1,3}\s[B]{1}[0-9]{1,2}\s[0-9]{1}";
            //var expressB2 = @"^\s{0,2}[0-9]{1,3}\s[FA]{2}[0-9]{1,2}\s[0-9]{1}";
            var expressW = @"^\s{0,2}[0-9]{1,3}\s[W]{1}[0-9]{1,2}\s[0-9]{1}";
            var expressEV1 = @"^\s{0,2}[0-9]{1,3}\s[EV1]{3}\s[0-9]{1}";
            var expressEV2 = @"^\s{0,2}[0-9]{1,3}\s[EV2]{3}\s[0-9]{1}";

            var allMembers = new List<MemberInfo>(); 

            var memberRange = GetRange(IDSTRING_MEMBERINFO);

            // Find all TC members in MEMBER INFO
            var allWebMembers = new List<MemberInfo>();

            for (int i = 0; i < memberRange.Count; i++)
            {
                Point start = null;
                Point end = null;

                if (Regex.IsMatch(memberRange[i], expressT) && memberRange[i + 3] != @"-1")
                {
                    var mem = new MemberInfo();
                    var array = Regex.Split(memberRange[i + 2], @",");
                    var subrange = memberRange[i].Substring(4).Split(' ');
                    var ptList = ExtractPointFromString(memberRange[i + 3]);

                    if (ptList.Count >= 2 && subrange[4] == "1")
                    {
                        var leftPoints = ptList.Take(Convert.ToInt16(array[11])).ToList();
                        var rightPoints = ptList.Where(p => !leftPoints.Contains(p)).ToList();                       
                        mem.SetLeftRightPointList(leftPoints, rightPoints);

                        if (mem.IsVertical)
                        {
                            var alignTypeStr = subrange[3];
                            var alignType = GetAlignType(alignTypeStr);
                            start = GetAlignPoint(mem.LeftPoints, alignType);
                            end = GetAlignPoint(mem.RightPoints, alignType);
                        }
                        else
                        {
                            start = mem.LeftPoints.OrderBy(p => p.X).First(p => p.Y == mem.LeftPoints.Max(pt => pt.Y));
                            end = mem.RightPoints.OrderByDescending(p => p.X).First(p => p.Y == mem.RightPoints.Max(pt => pt.Y));                            
                        }

                        mem.SetBeginEndPoints(start, end);
                        mem.Size = array[0].Split('x')[1];
                        mem.Name = subrange[0];
                        mem.Type = MemberType.TopChord;
                        allMembers.Add(mem);
                    }
                }

                if (Regex.IsMatch(memberRange[i], expressB) && memberRange[i + 3] != @"-1")
                {
                    var mem = new MemberInfo();
                    var array = Regex.Split(memberRange[i + 2], @",");
                    var subrange = memberRange[i].Substring(4).Split(' ');
                    var ptList = ExtractPointFromString(memberRange[i + 3]);

                    if (ptList.Count >= 2 && subrange[4] == "1")
                    {
                        var leftPoints = ptList.Take(Convert.ToInt16(array[11])).ToList();
                        var rightPoints = ptList.Where(p => !leftPoints.Contains(p)).ToList();
                        mem.SetLeftRightPointList(leftPoints, rightPoints);

                        if (mem.IsVertical)
                        {
                            var alignTypeStr = subrange[3];
                            var alignType = GetAlignType(alignTypeStr);
                            start = GetAlignPoint(mem.LeftPoints, alignType);
                            end = GetAlignPoint(mem.RightPoints, alignType);
                        }
                        else
                        {
                            start = mem.LeftPoints.OrderBy(p => p.X).First(p => p.Y == mem.LeftPoints.Min(pt => pt.Y));
                            end = mem.RightPoints.OrderByDescending(p => p.X).First(p => p.Y == mem.RightPoints.Min(pt => pt.Y));
                        }

                        mem.SetBeginEndPoints(start, end);
                        mem.Size = array[0].Split('x')[1];
                        mem.Name = subrange[0];
                        mem.Type = MemberType.BottomChord;
                        allMembers.Add(mem);
                    }
                }

                if (Regex.IsMatch(memberRange[i], expressW) && memberRange[i + 3] != @"-1")
                {
                    var mem = new MemberInfo();
                    var array = Regex.Split(memberRange[i + 2], @",");
                    var subrange = memberRange[i].Substring(4).Split(' ');
                    var ptList = ExtractPointFromString(memberRange[i + 3]);

                    if (ptList.Count >= 2 && subrange[4] == "1")
                    {
                        mem.Type = MemberType.Web;
                        var leftPoints = ptList.Take(Convert.ToInt16(array[11])).ToList();
                        var rightPoints = ptList.Where(p => !leftPoints.Contains(p)).ToList();
                        var alignTypeStr = subrange[3];
                        var alignType = GetAlignType(alignTypeStr);
                        mem.AlignType = alignType;
                        mem.SetLeftRightPointList(leftPoints, rightPoints);

                        start = GetAlignPoint(mem.LeftPoints, alignType);
                        end = GetAlignPoint(mem.RightPoints, alignType);                      
                        
                        mem.SetBeginEndPoints(start, end);
                        mem.Size = array[0].Split('x')[1];
                        mem.Name = subrange[0];
                        allMembers.Add(mem);
                    }
                }

                if (Regex.IsMatch(memberRange[i], expressEV1) && memberRange[i + 3] != @"-1")
                {
                    var mem = new MemberInfo();
                    var array = Regex.Split(memberRange[i + 2], @",");
                    var subrange = memberRange[i].Substring(4).Split(' ');
                    var ptList = ExtractPointFromString(memberRange[i + 3]);

                    if (ptList.Count >= 2 && subrange[4] == "1")
                    {
                        var leftPoints = ptList.Take(Convert.ToInt16(array[11])).ToList();
                        var rightPoints = ptList.Where(p => !leftPoints.Contains(p)).ToList();
                        var alignTypeStr = subrange[3];
                        var alignType = GetAlignType(alignTypeStr);
                        mem.AlignType = alignType;
                        mem.SetLeftRightPointList(leftPoints, rightPoints);

                        start = GetAlignPoint(mem.LeftPoints, alignType);
                        end = GetAlignPoint(mem.RightPoints, alignType);

                        mem.SetBeginEndPoints(start, end);
                        mem.Size = array[0].Split('x')[1];
                        mem.Name = subrange[0];
                        mem.Type = MemberType.EV1;
                        allMembers.Add(mem);
                    }
                }

                if (Regex.IsMatch(memberRange[i], expressEV2) && memberRange[i + 3] != @"-1")
                {
                    var mem = new MemberInfo();
                    var array = Regex.Split(memberRange[i + 2], @",");
                    var subrange = memberRange[i].Substring(4).Split(' ');
                    var ptList = ExtractPointFromString(memberRange[i + 3]);

                    if (ptList.Count >= 2 && subrange[4] == "1")
                    {
                        var leftPoints = ptList.Take(Convert.ToInt16(array[11])).ToList();
                        var rightPoints = ptList.Where(p => !leftPoints.Contains(p)).ToList();
                        var alignTypeStr = subrange[3];
                        var alignType = GetAlignType(alignTypeStr);
                        mem.AlignType = alignType;
                        mem.SetLeftRightPointList(leftPoints, rightPoints);

                        start = GetAlignPoint(mem.LeftPoints, alignType);
                        end = GetAlignPoint(mem.RightPoints, alignType);

                        mem.SetBeginEndPoints(start, end);
                        mem.Size = array[0].Split('x')[1];
                        mem.Name = subrange[0];
                        mem.Type = MemberType.EV2;
                        allMembers.Add(mem);
                    }
                }
            }

            AdjustMembers(allMembers);          

            // Remove duplicated members
            allMembers = allMembers.GroupBy(m => new { m.PointB, m.PointE.Y, m.Size }).Select(group => group.First()).ToList();

            return allMembers;
        }

        private void AdjustMembers(List<MemberInfo> allMembers)
        {
            var allWebs = allMembers.Where(m => m.Type == MemberType.Web).ToList();
            var allBcs = allMembers.Where(m => m.Type == MemberType.BottomChord).ToList();

            foreach (var web in allWebs)
            {
                bool b1 = false;
                bool b2 = false;
                bool b3 = false;

                foreach (var point in  web.LeftPoints)
	            {
                    b1 = allBcs.Any(ch => point.ShortestDistance2d(ch) <= Util.GetMemberActualWidth(ch.Size));
	            }

                foreach (var point in web.RightPoints)
                {
                    b2 = allBcs.Any(ch => point.ShortestDistance2d(ch) <= Util.GetMemberActualWidth(ch.Size));
                }
                                              

                if (b1 && b2)
                {
                    b3 = allBcs.Any(ch => ch.HasEndIntersect2d(web));

                    if (b3)
                    {
                        Point start, end;

                        if (web.IsVertical)
                        {
                            start = GetAlignPoint(web.LeftPoints, web.AlignType);
                            end = GetAlignPoint(web.RightPoints, web.AlignType);

                            web.SetBeginEndPoints(start, end);
                            web.Type = MemberType.BottomChord;
                        }                        
                    }                    
                }
            }

            // Check if Gap on TC

            var allTCMembers = allMembers.Where(m => m.Type == MemberType.TopChord).ToList();
            SortMembers(allTCMembers);

            for (int i = 0; i < allTCMembers.Count - 1; i++)
            {
                var tc1 = allTCMembers[i];
                var tc2 = allTCMembers[i + 1];

                if (!tc1.HasEndIntersect2d(tc2) && tc1.PointE.ShortestDistance2d(tc2) > Util.GetMemberActualWidth(tc1.Size))
                {
                    var startX = tc1.PointE.X;
                    var endX = tc2.PointB.X;

                    foreach (var member in allMembers)
                    {
                        if (member.LeftPoints.Any(p => Util.Equals(p.X, startX)) && member.RightPoints.Any(p => Util.Equals(p.X, endX)))
                        {
                            Point start, end;

                            if (member.IsVertical)
                            {
                                start = GetAlignPoint(member.LeftPoints, member.AlignType);
                                end = GetAlignPoint(member.RightPoints, member.AlignType);
                            }
                            else
                            {
                                start = member.LeftPoints.OrderBy(p => p.X).First(p => p.Y == member.LeftPoints.Max(pt => pt.Y));
                                end = member.RightPoints.OrderByDescending(p => p.X).First(p => p.Y == member.RightPoints.Max(pt => pt.Y));
                            }

                            member.SetBeginEndPoints(start, end);
                            member.Type = MemberType.TopChord;
                        }
                    }
                }
            }

            // Check if Gap on BC

            var allBCMembers = allMembers.Where(m => m.Type == MemberType.BottomChord).ToList();
            SortMembers(allBCMembers);

            for (int i = 0; i < allBCMembers.Count - 1; i++)
            {
                var mem1 = allBCMembers[i];
                var mem2 = allBCMembers[i + 1];

                if (!mem1.HasEndIntersect2d(mem2))
                {
                    var startX = mem1.RightPoints.Max(p => p.X);
                    var endX = mem2.LeftPoints.Min(p => p.X);

                    foreach (var member in allMembers)
                    {
                        if (member.LeftPoints.Any(p => Util.Equals(p.X, startX)) && member.RightPoints.Any(p => Util.Equals(p.X, endX)))
                        {
                            Point start, end;

                            if (member.IsVertical)
                            {
                                start = GetAlignPoint(member.LeftPoints, member.AlignType);
                                end = GetAlignPoint(member.RightPoints, member.AlignType);
                            }
                            else
                            {
                                start = member.LeftPoints.OrderBy(p => p.X).First(p => p.Y == member.LeftPoints.Min(pt => pt.Y));
                                end = member.RightPoints.OrderByDescending(p => p.X).First(p => p.Y == member.RightPoints.Min(pt => pt.Y));
                            }

                            member.SetBeginEndPoints(start, end);
                            member.Type = MemberType.BottomChord;
                        }
                    }
                }
            }
        }        

        private int ComputeQuantity()
        {
            // Truss quantity = Quantity / Ply
            return Convert.ToInt16(GetValue(m_AdditionalTrussInfoRange, "Quantity=", 1, '=')) / Convert.ToInt16(GetValue(m_AdditionalTrussInfoRange, "Ply=", 1, '='));
        }

        private RejectCause? ValidateTruss()
        {
            if (Convert.ToInt32(FileVersionString) < VERSION_SUPPORT_FROM)
            {
                return RejectCause.CAUSE_UNSUPPORT_VERSION;
            }

            if (!m_ReadLines.Any(line => line.StartsWith(IDSTRING_IMPERIALIMPERIAL)))
            {
                return RejectCause.CAUSE_UNIT_INVALID;
            }

            if (!m_ReadLines.Any(line => line.StartsWith(IDSTRING_ROOFBASICS)))
            {
                return RejectCause.CAUSE_UNSUPPORT_TRUSSTYPE;
            }

            if (IsValley || IsPiggyBack)
            {
                return RejectCause.CAUSE_UNSUPPORT_TRUSSTYPE;
            }

            return null;
        }

        private string GetVersion()
        {
            var chars = FileVersionString.ToCharArray();
            return string.Join(".", chars);
        }

        private string GetOverHangLeftRightCommand(bool left = true)
        {
            if (left)
            {
                var leftOH = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Left Overhang=", 1, '='));
                return Util.ToFeetInchSix(leftOH);
            }
            else
            {
                var rightOH = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Right Overhang=", 1, '='));
                return Util.ToFeetInchSix(rightOH);
            }            
        }

        /// <summary>
        /// Build webbing profile, only chord webs are accepted
        /// </summary>
        /// <returns></returns>
        private string BuildWebbingCommand()
        {
            var allWebMembers = m_AllMembers.Where(m => m.Type == MemberType.Web).ToList();

            if (!allWebMembers.Any())
            {
                return string.Empty;
            }

            // Remove non-chord webs
            for (int i = 0; i < allWebMembers.Count; )
            {
                var web = allWebMembers[i];

                if (!IsChordWeb(web))
                {
                    allWebMembers.Remove(web);
                    continue;
                }

                i++;
            }

            var TCJoints = new List<double>();
            var BCJoints = new List<double>();

            foreach (var web in allWebMembers)
            {
                if (IsNearTC(web.PointB))
                {
                    TCJoints.Add(web.PointB.X);
                    BCJoints.Add(web.PointE.X);
                }
                else
                {
                    TCJoints.Add(web.PointE.X);
                    BCJoints.Add(web.PointB.X);
                }
            }

            TCJoints = TCJoints.Distinct().ToList();
            BCJoints = BCJoints.Distinct().ToList();
            TCJoints.Sort((p1, p2) => p1.CompareTo(p2));
            BCJoints.Sort((p1, p2) => p1.CompareTo(p2));

            // TC Joints
            var span = Util.ToFeetInchSix(Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Max Span=", 1, '=')));
            var command = @"";

            for (int i = 0; i < TCJoints.Count; i++)
            {
                command += "x" + Util.ToFeetInchSix(TCJoints[i]) + " ";
            }

            if (TCJoints.Any() && Util.ToFeetInchSix(TCJoints[TCJoints.Count - 1]) != span)
            {
                command += "x" + span + " ";
            }

            // BC Joints
            for (int i = 0; i < BCJoints.Count; i++)
            {
                command += "x" + Util.ToFeetInchSix(BCJoints[i]) + " ";
            }

            if (BCJoints.Any() && Util.ToFeetInchSix(BCJoints[BCJoints.Count - 1]) != span)
            {
                command += "x" + span;
            }

            return command;
        }


        /// <summary>
        /// Check whether a Point is near TC or BC
        /// </summary>
        /// <param name="point">Point to check</param>
        /// <returns>True if near TC. False, near BC</returns>
        private bool IsNearTC(Point point)
        {
            double minDistance = point.ShortestDistance2d(m_TCProfiles[0]);
            bool isNearTC = true;

            foreach (var chord in m_TCProfiles)
            {
                var dist = point.ShortestDistance2d(chord);
                if (dist < minDistance)
                {
                    minDistance = dist;
                    isNearTC = true;
                }
            }

            foreach (var chord in m_BCProfiles)
            {
                var dist = point.ShortestDistance2d(chord);
                if (dist < minDistance)
                {
                    minDistance = dist;
                    isNearTC = false;
                }
            }

            return isNearTC;
        }

        private bool IsChordWeb(MemberInfo web)
        {
            var allEndPoints = new List<Point>();
            bool b1 = false;
            bool b2 = false;

            allEndPoints.AddRange(web.LeftPoints);
            allEndPoints.AddRange(web.RightPoints);
            
            foreach(var point in allEndPoints)
            {
                if (IsOnTC(point))
                {
                    b1 = true;
                    continue;
                }

                if (IsOnBC(point))
                {
                    b2 = true;
                    continue;
                }
            }

            return b1 && b2;          
        }

        private AlignType GetAlignType(string type)
        {
            if (type == "0")
            {
                return AlignType.Center;
            }
            else if (type == "1")
            {
                 return AlignType.Right;
            }
            else
            {
                return AlignType.Left;
            }
        }

        private Point GetAlignPoint(List<Point> points, AlignType alignType)
        {
            if (alignType == AlignType.Left)
            { 
                var xMin = points.Min(p => p.X);
                return points.FirstOrDefault(p => p.X == xMin);
            }

            if (alignType == AlignType.Right)
            {
                var xMax = points.Max(p => p.X);
                return points.FirstOrDefault(p => p.X == xMax);
            }

            if (points.Count() == 2)
            {
                var X = points.Sum(p => p.X) / 2;
                var Y = (points[0].Y + points[0].Y) / 2;
                return new Point(X, Y);
            }
            else 
            {
                return points.ElementAt(points.Count()/2);
            }            
        }        

        private string GetEndLeftRightDescriptionCommand(bool left = true)
        {            
            string endleft = @"";
            string endright = @"";

            bool EV1 = m_AllMembers.Any(m => m.Type == MemberType.EV1);
            bool EV2 = m_AllMembers.Any(m => m.Type == MemberType.EV2);
            bool hasTCBearing = GetBearingCommands().Any(brg => brg.Contains(TOPCHD));

            if (m_TCProfiles.Count == 1)
            {
                if (m_TCProfiles[0].Pitch > 0 && !EV2)
                {
                    endright = hasTCBearing ? @"N N N N N Y N N N N N 0 1 0 0 008 -100 400 1 508 0 0 ejrv tb 108" :
                                                  @"N N N N N Y N N N N N 0 1 0 0 008 -100 400 1 508 0 0 ejrv b 108";                    
                }
                else if (m_TCProfiles[0].Pitch < 0 && !EV1)
                {
                    endleft = hasTCBearing ? @"edl:N N N N N Y N N N N N 0 1 0 0 008 -100 400 1 508 0 0 ejrv tb 108" :
                                                @"N N N N N Y N N N N N 0 1 0 0 008 -100 400 1 508 0 0 ejrv b 108";
                }
            }

            return left ? endleft : endright;
        }                

        private string GetGableCommand()
        {
            var gableStr = @"";
            var isGable = GetValue(m_AdditionalTrussInfoRange, "Gable=", 1, '=') == @"YES";
            var isRegGable = GetValue(m_AdditionalTrussInfoRange, "Regular Gable=", 1, '=') == @"YES";
            var piggyFlag = GetValue(m_ReadLines, "Piggyback Settings:", 2, ' ') == @"1";
            var gableStudSpacing = Util.ToFeetInchSix(Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Gable Stud Spacing=", 1, '=')));
            var span = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Max Span=", 1, '='));

            if (isGable && isRegGable && !piggyFlag)
            {
                gableStr += @"N N 0 " + span + " N 0 " + span + " 308 " + gableStudSpacing + " 800 PeakOut None 2 2 004 Center Top None 007 Front 0 " + span + " Y";
            }

            return gableStr;
        }

        private string GetTCCommand()
        {
            var str = @"ha" + " " + Util.ToFeetInchSix(ComputeLeftHeelHeight());

            foreach (var item in m_TCProfiles)
            {                
                str += " " + item.ToString();
            }

            return str;
        }

        private string GetBCCommand()
        {
            var str = @"";

            foreach (var item in m_BCProfiles)
            {
                str += item.ToString() + " ";
            }

            return str;
        }        
        
        private List<string> GetBearingCommands()
        {
            var trussSpan = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Max Span=", 1, '='));
            int bearingIdx = m_ReadLines.IndexOf(IDSTRING_BEARINGINFO);
            int numberOfBearing = Convert.ToInt16(m_ReadLines[bearingIdx + 1].Split(' ').First());
            var bearingList = new List<string>();

            for (int i = 0; i < numberOfBearing; i++)
            {
                string[] arrBrg = Regex.Split(m_ReadLines[bearingIdx + 2 + i], @" ");
                string brgWSize = Util.ToFeetInchSix(Convert.ToDouble(arrBrg[2]));
                string brgEdge = BearingEdge(arrBrg[5]);
                string brgSymbol = BearingSymbol(arrBrg[9]);
                double brgXDist = Convert.ToDouble(arrBrg[3]);
                double brgX = Convert.ToDouble(arrBrg[3]);
                double brgY = Convert.ToDouble(arrBrg[4]);
                string brgXLocation = Util.ToFeetInchSix(Convert.ToDouble(arrBrg[3]));
                string brgYLocation = TOPCHD;

                if (brgEdge == @"RR")
                {
                    brgXDist = trussSpan - brgXDist;
                    brgXLocation = Util.ToFeetInchSix(brgXDist);
                }

                if (brgXLocation.Length == 3 && brgXLocation.Substring(0, 1) == "0")
                {
                    brgXLocation = "0" + brgXLocation;
                }

                for (int j = 0; j < m_BCProfiles.Count; j++)
                {
                    double pitch = m_BCProfiles[j].Pitch;
                    double xStr = m_BCProfiles[j].PointB.X;
                    double yStr = m_BCProfiles[j].PointB.Y;
                    double xEtr = m_BCProfiles[j].PointE.X;
                    double yEtr = m_BCProfiles[j].PointE.Y;

                    if (m_BCProfiles[j].IsVertical)
                    {
                        continue;
                    }
                    else if (brgY <= yStr || brgY <= yEtr)
                    {
                        brgYLocation = BOTCHD;
                        break;
                    }
                    else if (Math.Round((brgY - yStr) / (brgX - xStr) * 12.0, 0) == pitch)
                    {
                        brgYLocation = BOTCHD;
                        break;
                    }
                    else if (Math.Round((brgY - yEtr) / (brgX - xEtr) * 12.0, 0) == pitch)
                    {
                        brgYLocation = BOTCHD;
                        break;
                    }
                }

                string brg = "brg: " + brgWSize + " " + brgEdge + " " + brgXLocation + " " + brgYLocation + " Flat " + brgSymbol + " " + NOSPEC + " " + BearingMaterial(m_BearingMaterial) + " Y";
                bearingList.Add(brg);
            }

            return bearingList;
        }

        private SortedList<int, string> BuildIDStringList()
        {
            var expression = @"^\[?[a-zA-Z\s]+\]?$";

            var sortList = new SortedList<int, string>();

            for (int i = 0; i < m_ReadLines.Count; i++)
            {
                if (Regex.IsMatch(m_ReadLines[i], expression))
                {
                    sortList.Add(i, m_ReadLines[i]);
                }
            }

            return sortList;
        }

        private List<string> GetRange(string id)
        {
            if (m_ReadLines != null && m_ReadLines.Any() && m_IDStringDict != null && m_IDStringDict.Any())
            {
                int startIdx = m_IDStringDict.FirstOrDefault(pair => pair.Value == id).Key;
                int endIdx = m_IDStringDict.ElementAtOrDefault(m_IDStringDict.IndexOfValue(id) + 1).Key;

                return m_ReadLines.GetRange(startIdx, endIdx - startIdx);
            }

            return new List<string>();
        }

        private List<Point> BuildTCPoints()
        {
            var express1 = @"^(0\.000000 0\.000000)$";
            var express2 = @"^(-0\.000000 -0\.000000)$";
            int startTC = -1;
            int startBC = -1;
            var pointTCs = new List<Point>();

            var bearingInfoRange = GetRange(IDSTRING_BEARINGINFO);

            for (int i = 0; i < bearingInfoRange.Count; i++)
            {
                if ((Regex.IsMatch(bearingInfoRange[i], express1) || Regex.IsMatch(bearingInfoRange[i], express2)) && startTC == -1)
                {
                    startTC = i;  //TC Starting Point
                }
                else if (Regex.IsMatch(bearingInfoRange[i], express1) || Regex.IsMatch(bearingInfoRange[i], express2) && startBC == -1)
                {
                    startBC = i;  //BC Starting Point
                }
            }

            var count = startBC - startTC;
            var rangeTC = bearingInfoRange.GetRange(startTC, count > 0 ? count : 0);  //Get range of TC points            

            foreach (var str in rangeTC)
            {
                var splitStr = str.Split(' ');
                pointTCs.Add(new Point() { X = Convert.ToDouble(splitStr.First()), Y = Convert.ToDouble(splitStr.Last()) });
            }

            return pointTCs;
        }

        private List<ChordProfile> BuildTCProfiles()
        {
            var tcMembers = m_AllMembers.Where(m => m.Type == MemberType.TopChord).ToList();            
            var tcProfiles = new List<ChordProfile>();
            var pointsTC = BuildTCPoints(); // ----Since we can't detect Drop TC, so using TC points to build up TC profiles

            if (!tcMembers.Any())
            {
                return tcProfiles;
            }

            SortMembers(tcMembers);

            RemoveNonBeakingMembers(tcMembers);

            for (int i = 1; i < pointsTC.Count - 2; i++) // Loop through (n-2) points on TC line from Points_TC[1] to Points_TC[n-2]
            {
                if (i == 1)
                {
                    var chord = new ChordProfile();
                    chord.SetBeginEndPoints(pointsTC[i], pointsTC[i + 1]);
                    tcProfiles.Add(chord);
                }
                else
                {
                    var chord = new ChordProfile();
                    chord.SetBeginEndPoints(pointsTC[i], pointsTC[i + 1]);
                    int j = tcProfiles.Count - 1;
                    if (chord.Pitch == tcProfiles[j].Pitch && chord.PointB.Equals(tcProfiles[j].PointE))
                    {
                        tcProfiles[j].SetEndPoint(chord.PointE);
                    }
                    else
                    {
                        tcProfiles.Add(chord);
                    }
                }
            }

            // Adjust Member Size for TC profiles
            if (tcProfiles.Count == 1 && tcMembers.Any())
            {
                tcProfiles[0].Size = tcMembers[0].Size;
            }
            else
            {
                for (int i = 0; i < tcProfiles.Count; i++)
                {
                    double xLS = tcProfiles[i].PointB.X;
                    double xLE = tcProfiles[i].PointE.X;

                    for (int k = 0; k < tcMembers.Count; k++)
                    {
                        double xMid = 0.5 * (tcMembers[k].PointB.X + tcMembers[k].PointE.X);

                        if (Util.Equals(xLS, xMid))
                        {
                            xMid = xLS;
                        }

                        if (Util.Equals(xLE, xMid))
                        {
                            xMid = xLE;
                        }

                        if ((xMid > xLS && xMid < xLE) || (Util.Equals(xLS, xMid) && Util.Equals(xLE, xMid)))
                        {
                            tcProfiles[i].Size = tcMembers[k].Size;
                            break;
                        }
                    }
                }
            }            

            RemoveNonBeakingChords(tcProfiles);

            tcProfiles.Last().IsLast = true;

            tcProfiles.RemoveAll(ch => !ch.IsValid);

            return tcProfiles;
        }               

        private List<ChordProfile> BuildBCProfiles()
        {
            var bcMembers = m_AllMembers.Where(m => m.Type == MemberType.BottomChord).ToList();
            var bcProfiles = new List<ChordProfile>();

            SortMembers(bcMembers);
            RemoveNonBeakingMembers(bcMembers);

            // Build BC profile lines
            for (int i = 0; i < bcMembers.Count; i++)
            {
                var chord = new ChordProfile();
                var mem = bcMembers[i];

                var startPt = mem.PointB;
                var endPt = mem.PointE;

                if (i == 0)
                {
                    chord.IsFirst = true;
                }

                if (i == bcMembers.Count - 1)
                {
                    chord.IsLast = true;
                }

                if (mem.IsVertical && (i > 0 && i < bcMembers.Count - 1))
                {
                    var tempChord = new ChordProfile();
                    var start = bcMembers[i - 1].PointE;
                    var end = bcMembers[i + 1].PointB;
                    tempChord.SetBeginEndPoints(start, end);

                    if (tempChord.IsVertical)
                    {
                        startPt = start;
                        endPt = end;
                    }
                }

                chord.SetBeginEndPoints(startPt, endPt);
                chord.Size = mem.Size;
                bcProfiles.Add(chord);
            }

            RemoveGapOnChordProfiles(bcProfiles);

            RemoveNonBeakingChords(bcProfiles);

            // If a BC member has seat cut (left, right), divide its corresponding chord to two
            double dLeftSeatCut = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Left Seat Cut=", 1, '='));
            double dRightSeatCut = Convert.ToDouble(GetValue(m_AdditionalTrussInfoRange, "Right Seat Cut=", 1, '='));
            
            var twoPtsLeft = bcMembers[0].LeftPoints.OrderBy(p => p.Y).ThenBy(p => p.X).Take(2).ToList();
            var twoPtsRight = bcMembers[bcMembers.Count - 1].RightPoints.OrderBy(p => p.Y).ThenBy(p => p.X).Take(2).ToList();

            if (bcMembers[0].LeftPoints.Count > 2 && Util.Equals(twoPtsLeft[0].Y, twoPtsLeft[1].Y) && 
                Util.Equals(dLeftSeatCut, twoPtsLeft[1].Distance2d(twoPtsLeft[0])))
            {
                var chord = new ChordProfile();
                chord.SetBeginEndPoints(twoPtsLeft[0], twoPtsLeft[1]);
                chord.Size = bcProfiles[0].Size;
                bcProfiles[0].SetBeginEndPoints(chord.PointE, bcProfiles[0].PointE);
                chord.IsFirst = true;
                bcProfiles.First().IsFirst = false;
                bcProfiles.Insert(0, chord); // Left seat cut as the first chord
            }

            if (bcMembers.Last().RightPoints.Count > 2 && Util.Equals(twoPtsRight[0].Y, twoPtsRight[1].Y) && 
                Util.Equals(dRightSeatCut, twoPtsRight[1].Distance2d(twoPtsRight[0])))
            {
                var chord = new ChordProfile();
                chord.SetBeginEndPoints(twoPtsRight[0], twoPtsRight[1]);
                chord.Size = bcProfiles.Last().Size;
                bcProfiles.Last().SetBeginEndPoints(bcProfiles.Last().PointB, chord.PointB);
                chord.IsLast = true;
                bcProfiles.Last().IsLast = false;
                bcProfiles.Add(chord); // Right seat cut as the last chord
            }

            bcProfiles.RemoveAll(ch => !ch.IsValid);

            return bcProfiles;
        }

        private void RemoveNonBeakingMembers(List<MemberInfo> members)
        {
            for (int i = 0; i < members.Count - 1; )
            {
                var first = members[i];
                var second = members[i + 1];

                if (first.Pitch == second.Pitch &&
                    second.PointB.Equals(first.PointE) &&
                    second.Size == first.Size)
                {
                    first.SetLeftRightPointList(first.LeftPoints, second.RightPoints);
                    first.SetEndPoint(second.PointE);
                    members.Remove(second);

                    continue;
                }

                i++;
            }
        }

        private void RemoveNonBeakingChords(List<ChordProfile> chords)
        {
            for (int i = 0; i < chords.Count - 1; )
            {
                var first = chords[i];
                var second = chords[i + 1];

                if (first.Pitch == second.Pitch &&
                    second.PointB.Equals(first.PointE) &&
                    second.Size == first.Size)
                {
                    first.SetEndPoint(second.PointE);
                    chords.Remove(second);

                    continue;
                }

                i++;
            }
        }

        private void RemoveGapOnChordProfiles(List<ChordProfile> chordProfiles)
        {
            for (int i = 0; i < chordProfiles.Count - 1; )
            {
                var chord1 = chordProfiles[i];
                var chord2 = chordProfiles[i + 1];                               

                if (!chord1.HasEndIntersect2d(chord2))
                {
                    bool line_intersection;
                    bool segment_intersection;
                    Point intersectionPoint; Point closeP1; Point closeP2;

                    GlobalHelper.FindIntersection(chord1.PointB, chord1.PointE, chord2.PointB, chord2.PointE,
                        out line_intersection, out segment_intersection, out intersectionPoint, out closeP1, out closeP2);

                    if (segment_intersection || (!segment_intersection && line_intersection && chord1.PointE.Distance2d(chord2.PointB) > Util.GetMemberActualWidth(chord1.Size)))
                    {
                        var shortest2B = chord2.PointB.ShortestDistance2d(chord1);
                        var shortest2E = chord2.PointE.ShortestDistance2d(chord1);
                        var shortest1B = chord1.PointB.ShortestDistance2d(chord2);
                        var shortest1E = chord1.PointE.ShortestDistance2d(chord2);

                        if (shortest2B <= Util.GetMemberActualWidth(chord1.Size))
                        {
                            chord1.SetBeginEndPoints(chord1.PointB, intersectionPoint);
                            chord2.SetBeginEndPoints(intersectionPoint, chord2.PointE);
                            continue;
                        }
                        else if (shortest2E <= Util.GetMemberActualWidth(chord1.Size))
                        {
                            chord1.SetBeginEndPoints(chord1.PointB, intersectionPoint);
                            chord2.SetBeginEndPoints(intersectionPoint, chord2.PointB);
                            continue;
                        }
                        else if (shortest1B <= Util.GetMemberActualWidth(chord2.Size))
                        {
                            chord1.SetBeginEndPoints(chord1.PointE, intersectionPoint);
                            chord2.SetBeginEndPoints(intersectionPoint, chord2.PointE);
                            continue;
                        }
                        else if (shortest1E <= Util.GetMemberActualWidth(chord2.Size))
                        {
                            chord1.SetBeginEndPoints(chord1.PointB, intersectionPoint);
                            chord2.SetBeginEndPoints(intersectionPoint, chord2.PointE);
                            continue;
                        }
                    }

                    // Add a vertical chord to make chord1 and chord2 connected
                    var chord = new ChordProfile();
                    var startPt = chord1.PointE;
                    var endPt = new Point(chord1.PointE.X, chord2.PointB.Y);

                    chord.SetBeginEndPoints(startPt, endPt);
                    chord.Size = "4"; // The common size of a web

                    GlobalHelper.FindIntersection(chord.PointB, chord.PointE, chord2.PointB, chord2.PointE,
                        out line_intersection, out segment_intersection, out intersectionPoint, out closeP1, out closeP2);

                    if (segment_intersection)
                    {
                        // Adjust the second chord to make them connected
                        chord2.SetBeginEndPoints(chord.PointE, chord2.PointE);

                        // Insert the new chord to the right place in b/w 
                        chordProfiles.Insert(i + 1, chord);
                    }
                    else
                    {
                        // Seems that chord2 is not a valid chord, so remove it
                        chordProfiles.Remove(chord2);
                        continue;
                    }
                }

                i++;
            }
        }

        private void SortMembers(List<MemberInfo> list)
        {
            list.Sort((m1, m2) => m1.RightPoints.Max(p => p.X).CompareTo(m2.RightPoints.Max(p => p.X)));
            var lastMember = list.Last();

            list.Sort((m1, m2) => m1.LeftPoints.Min(p => p.X).CompareTo(m2.LeftPoints.Min(p => p.X)));

            if (list.Count < 3)
            {
                return;
            }

            list.Remove(lastMember);

            MemberInfo next = list[0];
            var copyList = new List<MemberInfo>();
            copyList.AddRange(list);
            list.Clear();

            do
            {
                var current = next;
                list.Add(current);
                copyList.Remove(current);

                if (!copyList.Any())
                {
                    break;
                }

                var nextlist1 = new List<Tuple<MemberInfo, Point>>();
                var nextlist2 = new List<Tuple<MemberInfo, double>>();

                foreach (var mem in copyList)
                {
                    bool lines_intersect;
                    bool segments_intersect; 
                    Point intersection;
                    Point close_p1;
                    Point close_p2;

                    GlobalHelper.FindIntersection(current.PointB, current.PointE, mem.PointB, mem.PointE, out lines_intersect, out segments_intersect, out intersection, out close_p1, out close_p2);

                    if (lines_intersect)
                    {
                        if ((Util.Equals(intersection.ShortestDistance2d(current), 0.0) && intersection.ShortestDistance2d(mem) <= Util.GetMemberActualWidth(current.Size)) ||
                            (Util.Equals(intersection.ShortestDistance2d(mem), 0.0) && intersection.ShortestDistance2d(current) <= Util.GetMemberActualWidth(mem.Size)))
                        {
                            nextlist1.Add(Tuple.Create(mem, intersection));
                        }
                        else
                        {
                            nextlist2.Add(Tuple.Create(mem, GlobalHelper.FindDistanceBetweenSegments(current.PointB, current.PointE, mem.PointB, mem.PointE)));
                        }
                    }
                    else 
                    {
                        nextlist2.Add(Tuple.Create(mem, GlobalHelper.FindDistanceBetweenSegments(current.PointB, current.PointE, mem.PointB, mem.PointE)));
                    }                    
                }

                if (nextlist1.Any())
                {
                    var shortestDistToB = nextlist1.Select(m => current.PointB.Distance2d(m.Item2)).OrderBy(x => x).First();
                    next = nextlist1.First(m => current.PointB.Distance2d(m.Item2) == shortestDistToB).Item1;
                }
                else
                {
                    nextlist2.Sort((m1, m2) => m1.Item2.CompareTo(m2.Item2));
                    next = nextlist2.First().Item1;
                }

            }
            while (copyList.Any());

            list.Add(lastMember);
        }

        private List<Point> ExtractPointFromString(string sInput)
        {
            string[] arrXY = Regex.Split(sInput, @",");

            var ptsList = new List<Point>();

            for (int i = 0; i < arrXY.Length; i+=2)
            {
                double xLoc = Convert.ToDouble(arrXY[i]);
                double yLoc = Convert.ToDouble(arrXY[i + 1]);
                Point XYLoc = new Point(xLoc, yLoc);
                ptsList.Add(XYLoc);
            }

            return ptsList;
        }        

        private string BearingMaterial(BearingMaterialEnum bme)
        {
            switch (bme)
            {
                case BearingMaterialEnum.DFL: return @"625";
                case BearingMaterialEnum.SP: return @"565";
                case BearingMaterialEnum.SPF: return @"425";
                case BearingMaterialEnum.HF: return @"405";
                default: return @"";
            }
        }

        private string BearingEdge(string sInput)
        {
            string sReturn = @"";

            if (sInput == "0")
            {
                sReturn = "LC";
            }
            else if (sInput == "1")
            {
                sReturn = "LL";
            }
            else
            {
                sReturn = "RR";
            }
                
            return sReturn;
        }

        private string BearingSymbol(string sInput)
        {
            string sReturn = @"";

            if (sInput == "0" || sInput == "1")
            {
                sReturn = "Wall";
            }
            else if (sInput == "2")
            {
                sReturn = "Hanger";
            }                
            else if (sInput == "3")
            {
                sReturn = "Beam";
            }                
            else if (sInput == "4")
            {
                sReturn = "Beam";
            }
            else if (sInput == "5")
            {
                sReturn = "ToeNail";
            }
                
            return sReturn;
        }      

        private string GetValue(List<string> range, string searchString, int index, params char[] separator)
        {
            return range.FirstOrDefault(line => line.StartsWith(searchString)).Split(separator)[index];
        }

        private double ComputeLeftHeelHeight()
        {
            var leftHH = 0.0;    

            if (Math.Abs(m_TCProfiles[0].PointB.X) == 0)
            {
                leftHH = m_TCProfiles[0].PointB.Y;
            }

            return leftHH;
        }

        private double ComputeRightHeelHeight()
        {
            var rightHH = 0.0;

            if (m_TCProfiles.Count == 1)
            {
                rightHH = m_TCProfiles[0].PointE.Y;
                return rightHH;
            }

            if (m_TCProfiles.Last().IsVertical)
            {
                rightHH = m_TCProfiles.Last().Dist;
            }
            else
            {
                rightHH = m_TCProfiles.Last().PointE.Y;
            }
     
            return rightHH;
        }

        /// <summary>
        /// Check if the point is on TC
        /// </summary>
        /// <param name="point">The point wanna check</param>
        /// <returns>True if point on TC. False, if not</returns>
        private bool IsOnTC(Point point)
        {
            double minDistance = point.ShortestDistance2d(m_TCProfiles[0]);
            var closestChord = m_TCProfiles[0];

            foreach (var chord in m_TCProfiles)
            {
                var dist = point.ShortestDistance2d(chord);
                if (dist < minDistance)
                {
                    minDistance = dist;
                    closestChord = chord;
                }
            }

            return Util.Equals(minDistance, Util.GetMemberActualWidth(closestChord.Size)) || minDistance < Util.GetMemberActualWidth(closestChord.Size);
        }

        /// <summary>
        /// Check if the point is on BC
        /// </summary>
        /// <param name="point">The point wanna check</param>
        /// <returns>True if point on BC. False, if not</returns>
        private bool IsOnBC(Point point)
        {
            double minDistance = point.ShortestDistance2d(m_BCProfiles[0]);
            var closestChord = m_BCProfiles[0];

            foreach (var chord in m_BCProfiles)
            {
                var dist = point.ShortestDistance2d(chord);
                if (dist < minDistance)
                {
                    minDistance = dist;
                    closestChord = chord;
                }
            }

            return Util.Equals(minDistance, Util.GetMemberActualWidth(closestChord.Size)) || minDistance < Util.GetMemberActualWidth(closestChord.Size);
        }

        #endregion Methods       
    }
}
