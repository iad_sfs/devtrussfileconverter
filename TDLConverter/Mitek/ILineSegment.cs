﻿namespace TDLConverter.Mitek
{
    public interface ILineSegment
    {
        Point PointB { get; set; }
        Point PointE { get; set; }
        bool IsVertical { get ; }
        double Pitch { get ; }
        double Dist { get ; }
    }
}
