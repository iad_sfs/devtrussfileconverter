﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Utils;

namespace TDLConverter.Mitek
{
    public class MemberInfo : ILineSegment
    {
        #region Properties

        public Point PointB { get; set; }
        public Point PointE { get; set; }                
        public string Name { get; set; }
        public string Size { get; set; }
        public MemberType Type { get; set; }
        public AlignType AlignType { get; set; }
        public List<Point> LeftPoints;
        public List<Point> RightPoints;

        public bool IsVertical
        {
            get
            { 
                foreach (var point in RightPoints)
	            {
                    if (LeftPoints.Any(p => Util.Equals(p.X, point.X)))
                    {
                        return true;
                    }
	            }

                return false;
            }
        }

        public double Pitch
        {
            get 
            {
                if (IsVertical)
                {
                    return GlobalHelper.INFINITY;                    
                }
                else
                {
                    return Math.Round((PointE.Y - PointB.Y) / (PointE.X - PointB.X) * 12, 2);
                }
            }
        }

        public double Dist 
        { 
            get
            {
                if (IsVertical)
                {
                    return (PointE.Y - PointB.Y);
                }
                else
                {
                    return (PointE.X - PointB.X);
                }
            }
        }

        #endregion Properties

        #region Methods

        public void SetBeginEndPoints(Point begin, Point end)
        {
            PointB = begin;
            PointE = end;
        }

        public void SetEndPoint(Point newEnd)
        {
            SetBeginEndPoints(PointB, newEnd);
        }

        public void SetLeftRightPointList(List<Point> list1, List<Point> list2)
        {           
            LeftPoints = list1.Min(p => p.X) <= list2.Min(p => p.X) ? list1 : list2;
            RightPoints = list1.Min(p => p.X) <= list2.Min(p => p.X) ? list2 : list1;
        }

        /// <summary>
        /// Check whether 2 members has end intersection
        /// </summary>
        /// <param name="other">other member</param>
        /// <returns></returns>
        public bool HasEndIntersect2d(MemberInfo other)
        {
            foreach (var point in LeftPoints)
            {
                if (other.LeftPoints.Any(p => Util.Equals(p.X, point.X) && Util.Equals(p.Y, point.Y)) || 
                    other.RightPoints.Any(p => Util.Equals(p.X, point.X) && Util.Equals(p.Y,point.Y)))
                {
                    return true;
                }
            }

            foreach (var point in RightPoints)
            {
                if (other.LeftPoints.Any(p => Util.Equals(p.X, point.X) && Util.Equals(p.Y, point.Y)) || 
                    other.RightPoints.Any(p => Util.Equals(p.X, point.X) && Util.Equals(p.Y, point.Y)))
                {
                    return true;
                }
            }



            return false;
        }

        #endregion Methods       
    }
}
