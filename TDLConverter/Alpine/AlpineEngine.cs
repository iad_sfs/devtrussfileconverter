﻿
using Interfaces;
using Models;
namespace TDLConverter.Alpine
{
    public class AlpineEngine : IParseEngine
    {
        public bool IsMatch(IContextObject item)
        {
            return false;
        }

        public ITDLObject Parse(IContextObject item)
        {
            item.IsRejected = true;
            item.CauseOfReject = RejectCause.CAUSE_UNKNOWN;
            return new TDLObject() { Title = item.FileNameNoExtension.ToUpper() };
        }
    }
}
