﻿
using TDLConverter.Mitek;
namespace TDLConverter
{
    public enum AlignType
    { 
        Center = 0,
        Right,
        Left
    }

    public enum MemberType
    { 
        TopChord,
        BottomChord,
        Web,
        EV1,
        EV2
    }

    public static class GlobalHelper
    {
        internal const double INFINITY = 999999;

        /// <summary>
        /// Get y-coordinate of point on a segment p1 --> p2
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GetY(Point p1, Point p2, double x)
        {
            if (p2.X == p1.X)
            {
                return double.NaN;
            }

            var m = (p2.Y - p1.Y) / (p2.X - p1.X);
            var b = p1.Y - (m * p1.X);

            return m * x + b;
        }

        /// <summary>
        /// Return the shortest distance between the two segments
        /// p1 --> p2 and p3 --> p4.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="p4"></param>
        /// <returns></returns>
        public static double FindDistanceBetweenSegments(Point p1, Point p2, Point p3, Point p4)
        {
            // See if the segments intersect.
            bool lines_intersect, segments_intersect;
            Point intersection;
            Point close1;
            Point close2;

            FindIntersection(p1, p2, p3, p4,
                out lines_intersect, out segments_intersect,
                out intersection, out close1, out close2);

            if (segments_intersect)
            {
                // They intersect.
                close1 = intersection;
                close2 = intersection;
                return 0;
            }

            // Find the other possible distances.
            double best_dist = double.MaxValue;
            double test_dist;

            // Try p1.
            test_dist = p1.ShortestDistance2d(new ChordProfile() { PointB = p3, PointE = p4 });

            if (test_dist < best_dist)
            {
                best_dist = test_dist;
            }

            // Try p2.
            test_dist = p2.ShortestDistance2d(new ChordProfile() { PointB = p3, PointE = p4 });

            if (test_dist < best_dist)
            {
                best_dist = test_dist;
            }

            // Try p3.
            test_dist = p3.ShortestDistance2d(new ChordProfile() { PointB = p1, PointE = p2 });

            if (test_dist < best_dist)
            {
                best_dist = test_dist;
            }

            // Try p4.
            test_dist = p4.ShortestDistance2d(new ChordProfile() { PointB = p1, PointE = p2 });

            if (test_dist < best_dist)
            {
                best_dist = test_dist;
            }

            return best_dist;
        }

        /// <summary>
        /// Find the point of intersection between the lines p1 --> p2 and p3 --> p4.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="p4"></param>
        /// <param name="lines_intersect">True if the lines containing the segments intersect</param>
        /// <param name="segments_intersect">True if the segments intersect</param>
        /// <param name="intersection">The point where the lines intersect</param>
        /// <param name="close_p1">The point on the first segment that is closest to the point of intersection</param>
        /// <param name="close_p2">The point on the second segment that is closest to the point of intersection</param>
        public static void FindIntersection(
            Point p1, Point p2, Point p3, Point p4,
            out bool lines_intersect, out bool segments_intersect,
            out Point intersection,
            out Point close_p1, out Point close_p2)
        {
            // Get the segments' parameters.
            double dx12 = p2.X - p1.X;
            double dy12 = p2.Y - p1.Y;
            double dx34 = p4.X - p3.X;
            double dy34 = p4.Y - p3.Y;

            // Solve for t1 and t2
            double denominator = (dy12 * dx34 - dx12 * dy34);

            double t1 = ((p1.X - p3.X) * dy34 + (p3.Y - p1.Y) * dx34) / denominator;

            if (double.IsInfinity(t1))
            {
                // The lines are parallel (or close enough to it).
                lines_intersect = false;
                segments_intersect = false;
                intersection = new Point(double.NaN, double.NaN);
                close_p1 = new Point(double.NaN, double.NaN);
                close_p2 = new Point(double.NaN, double.NaN);
                return;
            }
            lines_intersect = true;

            double t2 = ((p3.X - p1.X) * dy12 + (p1.Y - p3.Y) * dx12) / -denominator;

            // Find the point of intersection.
            intersection = new Point(p1.X + dx12 * t1, p1.Y + dy12 * t1);

            // The segments intersect if t1 and t2 are between 0 and 1.
            segments_intersect =
                ((t1 >= 0) && (t1 <= 1) &&
                 (t2 >= 0) && (t2 <= 1));

            // Find the closest points on the segments.
            if (t1 < 0)
            {
                t1 = 0;
            }
            else if (t1 > 1)
            {
                t1 = 1;
            }

            if (t2 < 0)
            {
                t2 = 0;
            }
            else if (t2 > 1)
            {
                t2 = 1;
            }

            close_p1 = new Point(p1.X + dx12 * t1, p1.Y + dy12 * t1);
            close_p2 = new Point(p3.X + dx34 * t2, p3.Y + dy34 * t2);
        }
    }    
}
