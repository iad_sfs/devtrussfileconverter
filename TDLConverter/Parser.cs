﻿using Interfaces;
using Models;
using System.Collections.Generic;
using System.Linq;
using TDLConverter.Alpine;
using TDLConverter.Eagle;
using TDLConverter.Mitek;
using TDLConverter.Truswal;

namespace Services
{
    public class Parser : IParser
    {        
        private readonly List<IParseEngine> m_parseEngines;

        public Parser()
        {
            m_parseEngines = new List<IParseEngine>() 
            {
                new MitekEngine(),
                new AlpineEngine(),
                new TruswalEngine(),
                new EagleEngine()
            };
        }
        
        public ITDLObject Parse(IContextObject item)
        {
            var matchEngine = m_parseEngines.FirstOrDefault(p => p.IsMatch(item));

            if (matchEngine != null)
            {
                return matchEngine.Parse(item);
            }

            item.IsRejected = true;
            item.CauseOfReject = RejectCause.CAUSE_UNKNOWN;
            return new TDLObject() { Title = item.FileNameNoExtension.ToUpper() };
        }
    }
}
