﻿
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Utils
{
    public class Util
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static readonly string MitekExtension = "*.tre";

        public static void SerializeToTDLFile(object obj, string filePath, string fileName)
        {
            var tmpPath = Path.GetTempFileName();
            var settings = new XmlWriterSettings { Encoding = Encoding.UTF8, Indent = true };

            var ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            var serializer = new XmlSerializer(obj.GetType());

            // note: FileMode.Create will overwrite existing
            using (XmlWriter xmlTextWriter = XmlWriter.Create(tmpPath, settings))
            {                
                serializer.Serialize(xmlTextWriter, obj, ns);
            }

            try
            {
                if (Directory.Exists(Path.GetFullPath(filePath)))
                {
                    var destination = Path.Combine(filePath, fileName);
                    File.Copy(tmpPath, destination, true);
                }
            }
            catch (IOException ex)
            {
                Log.Error("Could not create Truss Studio file " + fileName + "\n", ex);
            }

            try
            {
                // Delete the temp file
                File.Delete(tmpPath);
            }
            catch (IOException ex)
            {
                Log.Error("Could not delete file " + tmpPath + "\n", ex);
            }
        }       
               
        public static List<Tuple<string, string>> GetTdlEnvSettingMismatches(string localEnvDataPath, string UserEnvDataPath)
        {
            var ret = new List<Tuple<string, string>>();

            var localDoc = XDocument.Load(localEnvDataPath);
            localDoc.Root.Descendants().Where(e => e.Name == "UserInterface" || e.Name == "AuditControl").Remove();   // Skip UserInterface and AuditControl settings

            var userDoc = XDocument.Load(UserEnvDataPath);
            userDoc.Root.Descendants().Where(e => e.Name == "UserInterface" || e.Name == "AuditControl").Remove(); // Skip UserInterface and AuditControl settings

            var localTdlEnvNodes = localDoc.Root.Descendants().Where(e => e.Name == "tdlEnv");
            var userTdlEnvNodes = userDoc.Root.Descendants().Where(e => e.Name == "tdlEnv");

            var allLocalTdlEnvLeaves = localTdlEnvNodes.Descendants().Where(e => !e.Elements().Any());
            var allUserTdlEnvLeaves = userTdlEnvNodes.Descendants().Where(e => !e.Elements().Any());

            foreach (var lleaf in allLocalTdlEnvLeaves)
            {
                foreach (var uleaf in allUserTdlEnvLeaves)
                {
                    if (lleaf.Parent != null && uleaf.Parent != null && lleaf.Parent.Name == uleaf.Parent.Name && lleaf.Name == uleaf.Name)
                    {
                        var lValue = lleaf.Attributes().First(attr => attr.Name == "Value").Value;
                        var uValue = uleaf.Attributes().First(attr => attr.Name == "Value").Value;

                        if (lValue != uValue)
                        {
                            ret.Add(Tuple.Create(string.Format("{0}{1}", uleaf.Parent.Name, uleaf.Name), uValue));
                        }                        
                    }
                }
            }

            return ret;
        }

        // FFIISS
        public static string ToFeetInchSix(double dInput)
        {
            if (Equals(dInput, 0.0))
            {
                return "0";
            }               

            string s_in = @"";
            string s_si = @"";
            string s_Return = @"";
            if (dInput < 0)
            {
                s_Return += "-";
                dInput = Math.Abs(dInput);
            }
            int feet = 0;
            int inch = 0;
            int sixth = 0;
            double fttoin = 0.0;
            double intosix = 0.0;
            feet = (int)(dInput / 12.0);
            fttoin = dInput % 12;
            inch = (int)fttoin;
            intosix = fttoin % 1;
            sixth = Convert.ToInt32(Math.Round(intosix * 16, 0));

            if (sixth == 16)
            {
                sixth = 0;
                inch++;
            }

            if (inch == 12)
            {
                inch = 0;
                feet++;
            }

            if (sixth < 10)
            {
                s_si = sixth.ToString("00");
            }
            else
            {
                s_si = sixth.ToString();
            }

            if (inch < 10 && feet != 0)
            {
                s_in = inch.ToString("00");
            }
            else
            {
                s_in = inch.ToString();
            }

            if (s_in == @"00" && s_si == @"00")
            {
                s_Return += feet.ToString();
            }
            else if (feet == 0)
            {
                s_Return += s_in + s_si;
            }
            else
            {
                s_Return += feet.ToString() + s_in + s_si;
            }

            return s_Return;
        }

        // FF-II-SS
        public static string FormatFIS(double dInput)
        {
            string s_in = @"";
            string s_si = @"";
            string s_Return = @"";
            int feet = 0;
            int inch = 0;
            int sixth = 0;
            double fttoin = 0.0;
            double intosix = 0.0;
            feet = (int)(dInput / 12.0);
            fttoin = dInput % 12;
            inch = (int)fttoin;
            intosix = fttoin % 1;
            sixth = Convert.ToInt32(Math.Round(intosix * 16, 0));
            if (sixth == 16)
            {
                sixth = 0;
                inch++;
            }
            if (inch == 12)
            {
                inch = 0;
                feet++;
            }
            if (sixth < 10)
            {
                s_si = sixth.ToString("00");
            }
            else
            {
                s_si = sixth.ToString();
            }
            s_in = inch.ToString("00");

            if (s_in == @"00" && s_si == @"00" && feet == 0)
            {
                s_Return = @"0";
            }
            else
            {
                s_Return = feet.ToString() + "-" + s_in + "-" + s_si;
            }
            return s_Return;
        }

        public static bool Equals(double val1, double val2)
        {
            const double sixteeth = 0.0625;
            return Math.Abs(val1 - val2) < sixteeth;
        }

        /// <summary>
        /// Returns true if the string has illegal characters for a file name. A full path will return an error.
        /// </summary>
        /// <param name="fileName">The string to check</param>
        /// <returns></returns>
        public static bool HasIllegalFileNameChars(string fileName)
        {
            if (fileName.Trim() != fileName)
            {
                return true;
            }
            var invalidCharacters = Path.GetInvalidFileNameChars();
            if (fileName.IndexOfAny(invalidCharacters) != -1)
            {
                return true;
            }
            return false;
        }

        public static string LoadAssembly(string path)
        {
            try
            {
                var versionInfo = FileVersionInfo.GetVersionInfo(path);
                return string.Format("{0}.{1}.{2}.{3}", versionInfo.ProductMajorPart, versionInfo.ProductMinorPart, versionInfo.ProductBuildPart, versionInfo.ProductPrivatePart);
            }
            catch
            {
                return "0.0.0.0";
            }
        }

        public static double GetMemberActualWidth(string size)
        {
            switch (size)
            {
                case "3":
                    return 2.5;

                case "4":
                    return 3.5;

                case "6":
                    return 5.5;

                case "8":
                    return 7.25;

                case "10":
                    return 9.25;

                case "12":
                    return 11.25;

                default:
                    return 3.5;
            }
        }
    }
}
