﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using System.IO;
using System.Xml;
using Utils;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestMethod_SerializeToTDLFile()
        {           
            var doc = new XmlDocument();
            var tdlObject = doc.CreateElement("tdlObject");
            doc.AppendChild(tdlObject);

            tdlObject.SetAttribute("Title", "A01");
            tdlObject.SetAttribute("FileVersion", "16");
            tdlObject.SetAttribute("CompatibilityVersion", "2017.03.0.01");
            tdlObject.SetAttribute("Type", "Common");

            var scripts = doc.CreateElement("Scripts");
            
            tdlObject.AppendChild(scripts);

            var settings = doc.CreateElement("Settings");
            var country = doc.CreateElement("LocalizationCountry");
            country.SetAttribute("Value", "USA");
            settings.AppendChild(country);
            tdlObject.AppendChild(settings);

            ScriptDescriptor descriptor = new ScriptDescriptor()
            {
                Thickness = "thk:0108",
                Ply = "plys:1",
                Quantity = "mfg:2",
                Span = "span:271116",
                ProductionSpan = "prdspn:134141",
                LoadTemplate = "LoadTemplate: BBBB"
            };

            scripts.InnerText = descriptor.ToString();          

            Util.SerializeToTDLFile(doc, Path.GetFullPath("D:\\Source\\UnitTest"), "Test.tdl");
        }
    }
}
